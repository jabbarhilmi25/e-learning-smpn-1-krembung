<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_mhs extends CI_Controller {

	public function __construct(){
	parent::__construct();
	$this->load->model('M_matkul');
	$this->load->model('M_akun');
    $this->load->model('M_lead');

    
	}

    public function index(){
    $data['akun'] = $this->db->get_where('murid', ['id_murid' => $this->session->userdata('id_murid')])->row_array();

    $this->load->view('Mahasiswa/header_mhs',$data);
 	$this->load->view('Mahasiswa/v_home_mhs');
    $this->load->view('Mahasiswa/footer_mhs');
   
    }

    public function ganti(){
    $data['akun'] = $this->db->get_where('murid', ['id_murid' => $this->session->userdata('id_murid')])->row_array();

    $this->load->view('Mahasiswa/header_mhs',$data);
    $this->load->view('Mahasiswa/v_ganti_pass');
    $this->load->view('Mahasiswa/footer_mhs');
   
    }

    public function simpan_pass(){
    
    if($this->input->post('pass1') != $this->input->post('pass2') ) {
    $this->session->set_flashdata('message','<br> <div class="alert alert-danger" role="alert"><i class="ri-check-line"></i><i class="ri-lock-unlock-line"></i> Password tidak sama !
       </div>' );
    redirect(base_url('index.php/C_mhs/ganti'));
    // } else if( (8 <= strlen($this->input->post('pass1')) && (strlen($this->input->post('pass1') <= 16))) ) {
    //  $this->session->set_flashdata('message','<br> <div class="alert alert-danger" role="alert"><i class="ri-check-line"></i><i class="ri-lock-unlock-line"></i> Password tidak terdiri dari 8-16 karakter !
    //    </div>' );
    } else {
        $data = array(
            'id_murid'           => $this->session->userdata('id_murid'),
            'password_murid'     => md5($this->input->post('pass1')),
        );
    $this->M_akun->edit_murid($data);
    $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Password telah berhasil di ganti !
       </div>' );
    redirect(base_url('index.php/C_mhs'));
    }

    }//

    public function show_matkul($id){
        $data['mat']=$this->M_matkul->get_matkul();
        $data['kelas']=$this->M_matkul->get_kelas_byid($id);
        $data['akun'] = $this->db->get_where('murid', ['id_murid' => $this->session->userdata('id_murid')])->row_array();
        $this->load->view('Mahasiswa/header_mhs',$data);
        $this->load->view('Mahasiswa/v_matkul_mhs',$data);
        $this->load->view('Mahasiswa/footer_mhs');
    }

    public function kelas(){
        $data['kelas']=$this->M_matkul->get_kelas_byid($this->session->userdata('id_kelas'));
        $data['akun'] = $this->db->get_where('murid', ['id_murid' => $this->session->userdata('id_murid')])->row_array();
        $this->load->view('Mahasiswa/header_mhs',$data);
        $this->load->view('Mahasiswa/v_kelas',$data);
        $this->load->view('Mahasiswa/footer_mhs');
    }

    public function detail_matkul($id){
        $matkul = substr($id, 5);
        $kelas = substr($id,0,5);
        $data['mat']=$this->M_matkul->get_detail($matkul);
        $data['tugas']=$this->M_matkul->get_tugas($matkul,$kelas);
        $data['akun'] = $this->db->get_where('murid', ['id_murid' => $this->session->userdata('id_murid')])->row_array();
        $this->load->view('Mahasiswa/header_mhs',$data);
        $this->load->view('Mahasiswa/v_detail_matkul',$data);
        $this->load->view('Mahasiswa/footer_mhs');
    }

    public function detail_tugas($id){
        $data['js']     = 'mahasiswa/diskusi';
        $data['tugas']  = $this->M_matkul->detail_tugas($id);
        $data['akun']   = $this->db->get_where('murid', ['id_murid' => $this->session->userdata('id_murid')])->row_array();
        $data['id']     = $id;
        $this->load->view('Mahasiswa/header_mhs',$data);
        $this->load->view('Mahasiswa/v_detail_tugas',$data);
        $this->load->view('Mahasiswa/footer_mhs',$data);
    }

    
    public function upload_jawaban($id){
        $data = array(
                'id_tugas'          => $this->input->post('id'),
                'id_murid'           => $this->input->post('murid'),
                'id_matkul'         => $this->input->post('matkul'),
                'file_jawaban'      => $this->uploadFile(),
                'nilai_jawaban'     => '0',
                'tanggal_jawaban'   => date("Y-m-d H:i:s")
            ); 
            // var_dump($data);
            // die();
            $this->M_matkul->upload_jawaban($data);
            $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> File jawaban telah berhasil di tambahkan !
               </div>' );
            redirect(base_url('index.php/C_mhs/detail_tugas/'.$this->input->post('id')));
    }

    public function simpan_komen(){
        $data = array(
                'tgl_diskusi'       => date("Y-m-d H:i:s"),
                'isi_diskusi'       => $this->input->post('komen'),
                'id_akun'           => $this->session->userdata('id_murid'),
                'poin_diskusi'      => '05',
                'id_tugas'          => $this->input->post('id_tugas'),
            );

        $insert = $this->db->insert('diskusi',$data);

        if ($insert) {
            $output = array(
                'status' => true
            );
        } else {
            $output = array(
                'status' => false
            );
        }
        
        echo json_encode($output);
    }


    public function data_update()
    {
        $id = $this->input->post("id");

        $data['disk'] = $this->db->get_where('diskusi', array('id_diskusi' => $id))->row();

        $output = $this->load->view('Mahasiswa/form_update_komen',$data,true);

        echo json_encode($output);
    }


    public function edit_komen(){

        $id = $this->input->post("id");

        $data = array(
            'isi_diskusi'       => $this->input->post('komen2'),
        );

        $update = $this->db->update('diskusi',$data ,array('id_diskusi' => $id));

        if ($update) {
            $output = array(
                'status'    => true, 
                'pesan'     => "Komentar berhasil diupdate"
            );
        } else {
            $output = array(
                'status'    => false, 
                'pesan'     => "Gagal"
            );
        }
        
        echo json_encode($output);
    }

    private function uploadFile(){
            $config['upload_path']          = './uploadfile';
            $config['allowed_types']        = 'doc|docx|pdf|ppt|rar|zip';
            $config['file_name']            = $_FILES['file']['name'];
            $config['overwrite']            = true;
            // $config['max_size']             = 1024; // 1MB
    
            $this->load->library('upload', $config);
            
            if ($this->upload->do_upload('file')) {
                return $this->upload->data("file_name");
            }
        
            // return "default.jpg";
            print_r($this->upload->display_errors());
    }

    public function hapus_komen(){
        $id = $this->input->post('id_hapus');
        $hapus = $this->db->delete('diskusi',array('id_diskusi' => $id));

        if ($hapus) {
            $output = array(
                'status'    => true, 
                'pesan'     => "Komentar berhasil dihapus"
            );
        } else {
            $output = array(
                'status'    => false, 
                'pesan'     => "Gagal"
            );
        }
        
        echo json_encode($output);
        
        
        // $this->session->set_flashdata('message','<br> <div class="alert alert-secondary" role="alert"><i class="ri-check-line"></i> Komentar diskusi telah berhasil di dihapus !
        //        </div>' );
        // redirect(base_url('index.php/C_mhs/detail_tugas/'.$this->input->post('tugas2') ));

    }

    public function lead(){
        $data['mat']=$this->M_matkul->get_matkul();
        $data['akun'] = $this->db->get_where('murid', ['id_murid' => $this->session->userdata('id_murid')])->row_array();
        $this->load->view('Mahasiswa/header_mhs',$data);
        $this->load->view('Mahasiswa/v_lead',$data);
        $this->load->view('Mahasiswa/footer_mhs');
    }

    public function tes(){
        $this->load->view('Mahasiswa/v_tes');
    }

    public function get_diskusi()
    {
        $id = $this->input->post('id');

        $data['dis']=$this->M_matkul->diskusi($id);
        $data['akun'] = $this->db->get_where('murid', ['id_murid' => $this->session->userdata('id_murid')])->row_array();

        $output = $this->load->view('Mahasiswa/v_diskusi',$data,true);

        echo json_encode($output);
    }

    public function download($file){
        $this->load->helper('download');
        force_download('uploadfile/'.$file,NULL);
    }

}
?>
