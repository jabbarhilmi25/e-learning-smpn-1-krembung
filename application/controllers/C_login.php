<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {

	public function __construct(){
	parent::__construct();
	$this->load->model('M_akun');
	}

	public function index(){
		$this->load->view('header');
		$this->load->view('v_portal');
		$this->load->view('footer');
	}
	
	public function v_loginSiswa(){
		$this->load->view('Mahasiswa/v_login');
	}

	public function v_loginAdmin(){
		$this->load->view('Admin/v_login');
	}

	public function v_loginGuru(){
		$this->load->view('Dosen/v_login');
	}

	public function login_siswa(){
		
		$id=$this->input->post('nis');
		$pass=md5($this->input->post('pass'));
		$akun=$this->M_akun->get_idSiswa_byId($id);

		// var_dump($akun);
		// die();

		if($pass == $akun['password_murid']){
			$data = [
				'id_murid' => $akun['id_murid'],
				'id_kelas' => $akun['id_kelas'],
				'nama_murid' => $akun['nama_murid'],
				'password_murid' => $akun['password_murid'],
			];

			$this->session->set_userdata($data);
			redirect('C_mhs');
		}else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Password Salah ! Silahkan Coba Lagi ...
				</div>');
			redirect('C_login/v_loginSiswa');
		}
		

   }

	public function login_guru()
	{	

		$id=$this->input->post('nip');
		$pass=md5($this->input->post('pass'));
		$akun=$this->M_akun->get_idGuru_byId($id);

		// var_dump($akun);
		// die();

		if($pass == $akun['password_guru']){
			$data = [
				'id_guru' => $akun['id_guru'],
				'nama_guru' => $akun['nama_guru'],
				'password_guru' => $akun['password_guru'],
			];

			$this->session->set_userdata($data);
			if($akun['level']== "1"){
				redirect('C_admin');
			}else if($akun['level']== "2"){
				redirect('C_dosen');
			}
		}else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Password Salah ! Silahkan Coba Lagi ...
				</div>');
			redirect('C_login/v_loginguru');
		}

	}

	public function login_admin()
	{	

		$id=$this->input->post('id');
		$pass=md5($this->input->post('pass'));
		$akun=$this->M_akun->get_idAdmin_byId($id);

		if($pass == $akun['password_admin']){
			$data = [
				'id_admin' => $akun['id_admin'],
				'nama_admin' => $akun['nama_admin'],
				'password_admin' => $akun['password_admin'],
			];

			$this->session->set_userdata($data);
			redirect('C_admin');
		}else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"> Password Salah ! Silahkan Coba Lagi ...
				</div>');
			redirect('C_login/v_loginadmin');
		}

	}

	public function logout()
	{
		$this->session->unset_userdata('id_murid');
		$this->session->sess_destroy();
		$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            Anda berhasil logout !
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span></button></div>');
		redirect('C_login');
	}

}
?>
