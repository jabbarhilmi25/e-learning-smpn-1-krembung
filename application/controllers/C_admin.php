<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_admin extends CI_Controller {

	public function __construct(){
	parent::__construct();
	$this->load->model('M_matkul');
	$this->load->model('M_akun');

 //    if ($this->session->userdata('level') != '1') {
 //            $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
 //            Silahkan login terlebih dahulu !
 //            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
 //            <span aria-hidden="true">&times;</span></button></div>');
 //            redirect('C_login');
 //        }
	}

    public function index(){
    $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();

    $this->load->view('Admin/header_admin',$data);
 	$this->load->view('Admin/v_home_admin');
    $this->load->view('Admin/footer_admin');
    }

    public function akun(){
    $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
    // $data['acc'] = $this->M_akun->get_akun();

    $this->load->view('Admin/header_admin', $data);
 	$this->load->view('Admin/v_akun');
    $this->load->view('Admin/footer_admin');
    }

    public function tambah_akun(){
    $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
     $data['acc'] = $this->M_akun->get_akun();

    $this->load->view('Admin/header_admin',$data);
    $this->load->view('Admin/v_tambah_akun',$data);
    $this->load->view('Admin/footer_admin');
    }

    public function simpan_tambah(){
    $data = array(
            'id_akun'           => $this->input->post('id'),
            'password_akun'     => md5($this->input->post('id')),
            'nama_akun'         => $this->input->post('nama'),
            'level'             => $this->input->post('lvl'),
            'aku_ID_akun'       => $this->input->post('admin'),
        );

        $this->M_akun->tambah($data);
        $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Akun telah berhasil di tambahkan !
           </div>' );
        redirect(base_url('index.php/C_admin/akun'));
    }

    public function edit_akun($id){
    $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
    $data['acc'] = $this->M_akun->get_by_id($id);

    $this->load->view('Admin/header_admin',$data);
 	$this->load->view('Admin/v_edit_akun',$data);
    $this->load->view('Admin/footer_admin');
    }

    public function simpan_edit_akun(){
    $data = array(
             'id_akun'           => $this->input->post('id'),
             'nama_akun'         => $this->input->post('nama'),
             );

        $this->M_akun->edit($data);
        $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Akun telah berhasil di edit !
           </div>' );
        redirect(base_url('index.php/C_admin/akun'));
    }

    public function ganti(){
    $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();

    $this->load->view('Admin/header_admin',$data);
    $this->load->view('Admin/v_ganti_pass',$data);
    $this->load->view('Admin/footer_admin');
   
    }

    public function simpan_pass(){
    
    if($this->input->post('pass1') == $this->input->post('pass2') ) {
        if(strlen($this->input->post('pass1')) >= 8 AND strlen($this->input->post('pass1')) <= 16 ) {
        $data = array(
            'id_akun'           => $this->session->userdata('id_akun'),
            'password_akun'     => md5($this->input->post('pass1')),
        );
        $this->M_akun->edit($data);
        $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Password telah berhasil di ganti !
        </div>' );
        redirect(base_url('index.php/C_admin'));
        } else {
        $this->session->set_flashdata('message','<br> <div class="alert alert-danger" role="alert"><i class="ri-check-line"></i><i class="ri-lock-unlock-line"></i> Password harus 8-16 karakter !
            </div>' );
        redirect(base_url('index.php/C_admin/ganti')); 
        }

    // } else if( (8 <= strlen($this->input->post('pass1')) && (strlen($this->input->post('pass1') <= 16))) ) {
    //  $this->session->set_flashdata('message','<br> <div class="alert alert-danger" role="alert"><i class="ri-check-line"></i><i class="ri-lock-unlock-line"></i> Password tidak terdiri dari 8-16 karakter !
    //    </div>' );
    } else {
    $this->session->set_flashdata('message','<br> <div class="alert alert-danger" role="alert"><i class="ri-check-line"></i><i class="ri-lock-unlock-line"></i> Password tidak sama !
        </div>' );
     redirect(base_url('index.php/C_admin/ganti'));
    }

    }//

    public function kelas(){
        $data['kelas']=$this->M_matkul->get_kelas();
        $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        $this->load->view('admin/header_admin',$data);
        $this->load->view('admin/v_kelas',$data);
        $this->load->view('admin/footer_admin');
    }

    public function show_matkul($id){
        $data['mat']=$this->M_matkul->get_matkul();
        $data['kelas']=$this->M_matkul->get_kelas_byid($id);
        $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        $this->load->view('admin/header_admin',$data);
        $this->load->view('admin/v_matkul_admin',$data);
        $this->load->view('admin/footer_admin');
    }

    public function detail_matkul($id){
        $matkul = substr($id, 5);
        $kelas = substr($id,0,5);
        $data['mat']=$this->M_matkul->get_detail($matkul);
        $data['tugas']=$this->M_matkul->get_tugas($matkul,$kelas);
        $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        $this->load->view('admin/header_admin',$data);
        $this->load->view('admin/v_detail_matkul',$data);
        $this->load->view('admin/footer_admin');
    }

    public function detail_tugas($id){
        $data['tugas']=$this->M_matkul->detail_tugas($id);
        $data['dis']=$this->M_matkul->diskusi($id);
        $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        $this->load->view('admin/header_admin',$data);
        $this->load->view('admin/v_detail_tugas',$data);
        $this->load->view('admin/footer_admin');
    }

    public function tambah_matkul(){
        $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        $this->load->view('admin/header_admin',$data);
        $this->load->view('admin/v_tambah_matkul',$data);
        $this->load->view('admin/footer_admin');
    }

    public function simpan_matkul(){
    $data = array(
            'nama_matkul'   => $this->input->post('nama'),
            'sks'           => $this->input->post('sks')
        );

        $this->M_matkul->tambah($data);
        $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Mata Kuliah telah berhasil di tambahkan !
           </div>' );
        redirect(base_url('index.php/C_admin/show_matkul'));
    }

    public function edit_matkul($id){
        $data['mat']=$this->M_matkul->get_detail($id);
        $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        $this->load->view('admin/header_admin',$data);
        $this->load->view('admin/v_edit_matkul',$data);
        $this->load->view('admin/footer_admin');
    }

    public function simpan_edit(){
    $data = array(
            'id_matkul'     => $this->input->post('id'),
            'nama_matkul'   => $this->input->post('nama'),
            'sks'           => $this->input->post('sks')
        );

        $this->M_matkul->edit($data);
        $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Mata Kuliah telah berhasil di edit !
           </div>' );
        redirect(base_url('index.php/C_admin/show_matkul'));
    }

    public function show_nilai(){
        $data['mat']=$this->M_matkul->get_matkul();
        $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        $this->load->view('Admin/header_admin',$data);
        $this->load->view('Admin/v_nilai',$data);
        $this->load->view('Admin/footer_admin');
    }

    public function detail_nilai($id){
        $data['mat']=$this->M_matkul->get_detail($id);
        $data['tugas']=$this->M_matkul->get_nilai_tugas($id);
        $data['acc']=$this->M_matkul->get_akun();
        // $data['nilai']=$this->M_matkul->get_nilai($id);
        $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        $this->load->view('Admin/header_admin',$data);
        $this->load->view('Admin/v_detail_nilai',$data);
        $this->load->view('Admin/footer_admin');
    }

    public function cari_nilai($id){
        $nama =$this->input->get('cari');
        $data['mat']=$this->M_matkul->get_detail($id);
        $data['tugas']=$this->M_matkul->get_nilai_tugas($id);
        $data['acc']= $this->M_matkul->get_akun_by_id($nama);
        // $data['nilai']=$this->M_matkul->get_nilai($id);
        $data['akun'] = $this->db->get_where('admin', ['id_admin' => $this->session->userdata('id_admin')])->row_array();
        $this->load->view('Admin/header_admin',$data);
        $this->load->view('Admin/v_detail_nilai',$data);
        $this->load->view('Admin/footer_admin');
    }
}
?>
