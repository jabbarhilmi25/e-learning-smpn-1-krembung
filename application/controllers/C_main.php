<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_main extends CI_Controller {

    public function __construct(){
        parent::__construct();
    }

    public function index(){
     $this->load->view('header');
     $this->load->view('v_index');
     $this->load->view('footer');
    }

    public function coba(){
     $this->load->view('header');
     $this->load->view('v_index2');
     $this->load->view('footer');
    }

    public function v_dosen()   {
     $this->load->view('header');
     $this->load->view('Dosen/v_dosen');
     $this->load->view('footer');
    }

    public function v_matkul_dosen(){
     $this->load->view('header');
     $this->load->view('Dosen/v_matkul_dosen');
     $this->load->view('footer');
    }
}
?>