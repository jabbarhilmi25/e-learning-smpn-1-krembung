<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class C_dosen extends CI_Controller{

    public function __construct()
    {
    	parent::__construct();
    	$this->load->model('M_matkul');
        $this->load->model('M_akun');
        $this->load->model('M_lead');

        // if ($this->session->userdata('level') != '2') {
        //     $this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        //     Silahkan login terlebih dahulu !
        //     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        //     <span aria-hidden="true">&times;</span></button></div>');
        //     redirect('C_login');
        // }
    }

    public function index(){
    $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();

    $this->load->view('Dosen/header_dosen',$data);
    $this->load->view('Dosen/v_dosen',$data);
    $this->load->view('Dosen/footer_dosen');
    }

    public function show_matkul($id){
        $data['id']=$id;
        $data['mat']=$this->M_matkul->get_matkul_byid($id);
        $data['kelas']=$this->M_matkul->get_kelas_byid($id);
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_matkul_dosen',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function kelas(){
        $data['kelas']=$this->M_matkul->get_kelas();
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_kelas',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function detail_matkul($id){
        $matkul = substr($id, 5);
        $kelas = substr($id,0,5);
        $data['id'] = $id;
        $data['mat']=$this->M_matkul->get_detail($matkul);
        $data['tugas']=$this->M_matkul->get_tugas($matkul,$kelas);
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_detail_matkul',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function tambah_matkul(){
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_tambah_matkul',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function simpan_matkul(){
    $data = array(
            'nama_matkul'   => $this->input->post('nama'),
            'jam_belajar'   => $this->input->post('jam'),
        );

        $this->M_matkul->tambah($data);
        $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Mata Pelajaran telah berhasil di tambahkan !
           </div>' );
        redirect(base_url('index.php/C_dosen/kelas'));
    }

    public function simpan_kelas(){
    $data = array(
            'nama_kelas'   => $this->input->post('nama'),
            'id_guru'      => $this->input->post('list'),
            'gambar'       => $this->uploadGambar()
        );

        $this->M_matkul->tambah_kelas($data);
        $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Kelas telah berhasil di tambahkan !
           </div>' );
        redirect(base_url('index.php/C_dosen/kelas'));
    }

    public function edit_matkul($id){
        $data['mat']=$this->M_matkul->get_detail($id);
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_edit_matkul',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function simpan_edit(){
    $data = array(
            'id_matkul'     => $this->input->post('id'),
            'nama_matkul'   => $this->input->post('nama')
        );

        $this->M_matkul->edit($data);
        $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Mata Kuliah telah berhasil di edit !
           </div>' );
        redirect(base_url('index.php/C_dosen/kelas'));
    }

    public function ganti(){
    $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();

    $this->load->view('Dosen/header_dosen',$data);
    $this->load->view('Dosen/v_ganti_pass');
    $this->load->view('Dosen/footer_dosen');

    }

    public function simpan_pass(){

    if($this->input->post('pass1') != $this->input->post('pass2') ) {
    $this->session->set_flashdata('message','<br> <div class="alert alert-danger" role="alert"><i class="ri-check-line"></i><i class="ri-lock-unlock-line"></i> Password tidak sama !
       </div>' );
    redirect(base_url('index.php/C_dosen/ganti'));
    // } else if( (8 <= strlen($this->input->post('pass1')) && (strlen($this->input->post('pass1') <= 16))) ) {
    //  $this->session->set_flashdata('message','<br> <div class="alert alert-danger" role="alert"><i class="ri-check-line"></i><i class="ri-lock-unlock-line"></i> Password tidak terdiri dari 8-16 karakter !
    //    </div>' );
    } else {
        $data = array(
            'id_guru'           => $this->session->userdata('id_guru'),
            'password_guru'     => md5($this->input->post('pass1')),
        );
    $this->M_akun->edit_guru($data);
    $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Password telah berhasil di ganti !
       </div>' );
    redirect(base_url('index.php/C_dosen'));
    }

    }//

    private function uploadFile($judul){
            $config['upload_path']          = './uploadfile';
            $config['allowed_types']        = 'doc|docx|pdf|ppt|rar|zip';
            $config['file_name']            = $_FILES['file']['name'];
            $config['overwrite']            = true;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                return $this->upload->data("file_name");
            }

            // return "default.jpg";
            print_r($this->upload->display_errors());

    }

    private function uploadGambar(){
            $config['upload_path']          = './uploadfile';
            $config['allowed_types']        = 'jpg|png|jpeg';
            $config['file_name']            = $_FILES['file']['name'];
            $config['overwrite']            = true;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('file')) {
                return $this->upload->data("file_name");
            }

            // return "default.jpg";
            print_r($this->upload->display_errors());

    }

    public function detail_tugas($id){
        $data['id']     = $id;
        $data['js']    = 'dosen/diskusi';
        $data['tugas']=$this->M_matkul->detail_tugas($id);
        $data['dis']=$this->M_matkul->diskusi($id);
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_detail_tugas',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function tambah_tugas($id){
        $data['id']= $id;
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $data['id_matkul'] = $id;
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_tambah_tugas',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function simpan_tugas(){
        $data = array(
            'id_kelas'          => $this->input->post('kelas'),
            'id_tugas'          => $this->input->post('tugas'),
            'judul_tugas'       => $this->input->post('judul'),
            'tanggal_tugas'     => date("Y-m-d H:i:s"),
            'deadline'          => $this->input->post('deadline'),
            'file_tugas'        => $this->uploadFile($this->input->post('judul')),
            'deskripsi_tugas'   => $this->input->post('deskripsi'),
            'id_matkul'         => $this->input->post('matkul')
        );

        // var_dump($data);
        // die();

        $this->M_matkul->tambah_tugas($data);
        $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Tugas telah berhasil di tambahkan !
           </div>' );
        redirect(base_url('index.php/C_dosen/detail_matkul/'.$this->input->post('id')));
    }

    public function edit_tugas($id){
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $data['tugas']=$this->M_matkul->detail_tugas($id);
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_edit_tugas',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function simpan_edit_tugas(){
    $data = array(
            'id_matkul'         => $this->input->post('matkul'),
            'id_tugas'          => $this->input->post('id'),
            'id_kelas'          => $this->input->post('kelas'),
            'judul_tugas'       => $this->input->post('judul'),
            'deadline'          => $this->input->post('deadline'),
            'deskripsi_tugas'   => $this->input->post('deskripsi'),
            'file_tugas'        => $this->uploadFile($this->input->post('judul')),
        );

        $this->M_matkul->edit_tugas($data);
        $this->session->set_flashdata('message','<div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Tugas telah berhasil di edit !
           </div>' );
        redirect(base_url('index.php/C_dosen/detail_matkul/'.$this->input->post('kelas').$this->input->post('matkul')));
    }

    public function download($file){
        $this->load->helper('download');
        force_download('uploadfile/'.$file,NULL);
    }

    public function harga($biaya){
        // $jumlah = random_int();
        // return $biaya/$jumlah;
    }

    public function get_diskusi()
    {
        $id = $this->input->post('id');

        $data['dis']=$this->M_matkul->diskusi($id);
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();

        $output = $this->load->view('Dosen/v_diskusi',$data,true);

        echo json_encode($output);
    }

    public function simpan_komen(){
        $data = array(
                'tgl_diskusi'       => date("Y-m-d H:i:s"),
                'isi_diskusi'       => $this->input->post('komen'),
                'id_akun'           => $this->session->userdata('id_akun'),
                'poin_diskusi'      => '05',
                'id_tugas'          => $this->input->post('id_tugas'),
            );

        $insert = $this->db->insert('diskusi',$data);

        if ($insert) {
            $output = array(
                'status' => true
            );
        } else {
            $output = array(
                'status' => false
            );
        }

        echo json_encode($output);
    }


    public function data_update()
    {
        $id = $this->input->post("id");

        $data['disk'] = $this->db->get_where('diskusi', array('id_diskusi' => $id))->row();

        $output = $this->load->view('Dosen/form_update_komen',$data,true);

        echo json_encode($output);
    }


    public function edit_komen(){

        $id = $this->input->post("id");

        $data = array(
            'isi_diskusi'       => $this->input->post('komen2'),
        );

        $update = $this->db->update('diskusi',$data ,array('id_diskusi' => $id));

        if ($update) {
            $output = array(
                'status'    => true,
                'pesan'     => "Komentar berhasil diupdate"
            );
        } else {
            $output = array(
                'status'    => false,
                'pesan'     => "Gagal"
            );
        }

        echo json_encode($output);
    }

    public function hapus_komen(){
        $id = $this->input->post('id_hapus');
        $hapus = $this->db->delete('diskusi',array('id_diskusi' => $id));

        if ($hapus) {
            $output = array(
                'status'    => true,
                'pesan'     => "Komentar berhasil dihapus"
            );
        } else {
            $output = array(
                'status'    => false,
                'pesan'     => "Gagal"
            );
        }

        echo json_encode($output);


        // $this->session->set_flashdata('message','<br> <div class="alert alert-secondary" role="alert"><i class="ri-check-line"></i> Komentar diskusi telah berhasil di dihapus !
        //        </div>' );
        // redirect(base_url('index.php/C_mhs/detail_tugas/'.$this->input->post('tugas2') ));

    }

    // public function simpan_komen(){
    // $data = array(
    //         'tgl_diskusi'       => date("Y-m-d H:i:s"),
    //         'isi_diskusi'       => $this->input->post('komen'),
    //         'id_akun'           => $this->input->post('akun'),
    //         'id_tugas'          => $this->input->post('tugas')
    //     );

    //     $this->M_matkul->tambah_komen($data);
    //     $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Diskusi telah berhasil di tambahkan !
    //        </div>' );
    //     redirect(base_url('index.php/C_dosen/detail_tugas/'.$this->input->post('tugas')));
    // }

    // public function edit_komen($id){
    //     $data = array(
    //             'id_diskusi'        => $id,
    //             'tgl_diskusi'       => date("Y-m-d H:i:s"),
    //             'isi_diskusi'       => $this->input->post('komen2'),
    //             'id_akun'           => $this->input->post('akun2'),
    //             'id_tugas'          => $this->input->post('tugas2'),
    //             'id_matkul'         => $this->input->post('matkul2')
    //         );

    //         $this->M_matkul->update_komen($data);
    //         $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Diskusi telah berhasil di edit !
    //            </div>' );
    //         redirect(base_url('index.php/C_dosen/detail_tugas/'.$this->input->post('tugas2')));
    // }

    // public function hapus_komen($id){

    //     $data = array(  'id_diskusi'     =>  $id
    //                 );
    //     $this->M_matkul->delete_komen($data);
    //     $this->session->set_flashdata('message','<br> <div class="alert alert-secondary" role="alert"><i class="ri-check-line"></i> Komentar diskusi telah berhasil di dihapus !
    //            </div>' );
    //     redirect(base_url('index.php/C_dosen/detail_tugas/'.$this->input->post('tugas2') ));

    // }

    public function poin_komen(){
        $id = $this->input->post('id');

        $data = array(
            'poin_diskusi'       => $this->input->post('poin'),
        );

        $update = $this->db->update('diskusi',$data,array('id_diskusi' => $id) );

        if ($update) {
            $output = array(
                'status'    => true,
                'pesan'     => "Point diskusi berhasil ditambahkan"
            );
        } else {
            $output = array(
                'status'    => false,
                'pesan'     => "Gagal"
            );
        }

        echo json_encode($output);
    }

    public function pengumpulan($id){
        $data['tugas']=$this->M_matkul->detail_tugas($id);
        $data['dis']=$this->M_matkul->diskusi($id);
        $data['jawab']=$this->M_matkul->jawaban($id);
        $data['jwb']=$this->M_matkul->jawaban($id);
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_pengumpulan',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function nilai(){
        $data = array(
                'id_jawaban'         => $this->input->post('id'),
                'nilai_jawaban'      => $this->input->post('nilai'),
            );

            $this->M_matkul->update_nilai($data);
            $this->session->set_flashdata('message','<br> <div class="alert alert-primary" role="alert"><i class="ri-check-line"></i> Nilai telah berhasil di tambahkan !
               </div>' );
            redirect(base_url('index.php/C_dosen/pengumpulan/'.$this->input->post('tugas')));
    }

    public function show_nilai(){
        $data['mat']=$this->M_matkul->get_matkul();
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_nilai',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function detail_nilai($id){
        $data['mat']=$this->M_matkul->get_detail($id);
        $data['tugas']=$this->M_matkul->get_nilai_tugas($id);
        $data['acc']=$this->M_matkul->get_akun();
        // $data['nilai']=$this->M_matkul->get_nilai($id);
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_detail_nilai',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function cari_nilai($id){
        $nama =$this->input->get('cari');
        $data['mat']=$this->M_matkul->get_detail($id);
        $data['tugas']=$this->M_matkul->get_nilai_tugas($id);
        $data['acc']= $this->M_matkul->get_akun_by_id($nama);
        // $data['nilai']=$this->M_matkul->get_nilai($id);
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_detail_nilai',$data);
        $this->load->view('Dosen/footer_dosen');
    }

    public function lead(){
        $data['mat']=$this->M_matkul->get_matkul();
        $data['akun'] = $this->db->get_where('guru', ['id_guru' => $this->session->userdata('id_guru')])->row_array();
        $this->load->view('Dosen/header_dosen',$data);
        $this->load->view('Dosen/v_lead',$data);
        $this->load->view('Dosen/footer_dosen');
    }

}
?>
