 <!-- ======= Footer ======= -->
 <footer id="footer" class="footer">

    

<div class="footer-top">
  <div class="container">
    <div class="row gy-4">
      <div class="col-lg-5 col-md-12 footer-info">
        <!-- <a href="index.html" class="logo d-flex align-items-center"> -->
        <img src="<?= base_url('template/assets/img/logoSmp.png')?>" style="height: 150px;width: 150px"> 
            <br> <br> 
            <!-- </a> -->
          
           <p> <i class="bi bi-geo-alt-fill"> Ds. Mojoruntut, Krembung, Sidoarjo </i> </p>
           <p> <i class="bi bi-telephone-fill"> (031)8850795 </i> </p>
           <p> <i class="bi bi-envelope-fill">smpn1krembung_sidoarjo@yahoo.co.id </i></p>
           <p> <i class="bi bi-globe">www.smpn1krembung.mysch.id</i></p>
      
        <!-- <div class="social-links mt-3">
          <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
          <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
          <a href="#" class="instagram"><i class="bi bi-instagram bx bxl-instagram"></i></a>
          <a href="#" class="linkedin"><i class="bi bi-linkedin bx bxl-linkedin"></i></a>
        </div> -->
      </div>

      <!-- <div class="col-lg-2 col-6 footer-links">
        <h4>Lembaga</h4>
        <ul>
          <li><i class="bi bi-chevron-right"></i> <a href="https://lik.umsida.ac.id/">Al-Islam dan Kemuhammadiyahan</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://lsp.umsida.ac.id/">Sertifikasi Profesi</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://bahasa.umsida.ac.id/">Pusat Bahasa</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://library.umsida.ac.id/">Perpustakaan</a></li>
          
        </ul>
      </div>

      <div class="col-lg-2 col-6 footer-links">
        <h4>Direktorat</h4>
        <ul>
          <li><i class="bi bi-chevron-right"></i> <a href="https://akademik.umsida.ac.id/">Akademik</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://dkb.umsida.ac.id/">Keuangan dan Bisnis</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://dpsdm.umsida.ac.id/">Pengembangan SDM</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://dpal.umsida.ac.id/">Pengelolaan Aset dan Lingkungan</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://dsti.umsida.ac.id/">Sistem dan Teknologi Informasi</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://drpm.umsida.ac.id/">Riset dan Pengabdian Masyarakat</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://dkka.umsida.ac.id/">Kerjasama, Kemahasiswaan dan Alumni</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://suui.umsida.ac.id/">Sekretariatan Universitas dan Urusan Internasional</a></li>
        </ul>
      </div>

      <div class="col-lg-2 col-6 footer-links">
        <h4>Pengawas Internal</h4>
        <ul>
          <li><i class="bi bi-chevron-right"></i> <a href="https://bpm.umsida.ac.id/">Badan Penjaminan Mutu</a></li>
          <li><i class="bi bi-chevron-right"></i> <a href="https://spi.umsida.ac.id/">Satuan Pengawas Internal</a></li>
        </ul>
      </div> -->
    </div>
  </div>
</div>


</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"> </script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"> </script>
<script src="<?= base_url('comment/js/jquery-comments.js')?>"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.textcomplete/1.8.4/jquery.textcomplete.min.js"></script>
<script src="<?= base_url('template/assets/vendor/bootstrap/js/bootstrap.bundle.js')?>"></script>
<script src="<?= base_url('template/assets/vendor/aos/aos.js')?>"></script>
<script src="<?= base_url('template/assets/vendor/php-email-form/validate.js')?>"></script>
<script src="<?= base_url('template/assets/vendor/swiper/swiper-bundle.min.js')?>"></script>
<script src="<?= base_url('template/assets/vendor/purecounter/purecounter.js')?>"></script>
<script src="<?= base_url('template/assets/vendor/isotope-layout/isotope.pkgd.min')?>"></script>
<script src="<?= base_url('template/assets/vendor/glightbox/js/glightbox.min.js')?>"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="<?= base_url('template/assets/toastr/toastr.min.js')?>"></script>

<!-- Template Main JS File -->
<script src="<?= base_url('template/assets/js/main.js')?>"></script>
<?php if (isset($js)): ?>
  <script src="<?= base_url('template/assets/js/'.$js.'.js') ?>"></script>
<?php endif ?>
</body>
<script>
    var base_url = "<?php echo base_url(); ?>";
    toastr.options.closeButton = true;
    toastr.options.progressBar = true;
    toastr.options.timeOut = 3000;
    toastr.options.preventDuplicates = false;
    toastr.options.positionClass = "toast-bottom-right";
</script>

</body>

</html>