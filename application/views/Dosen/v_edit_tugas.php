<section id="features" class="features">

  <br><br><br>
  <div class="container" data-aos="fade-up">

        <header class="section-header">
          <p>Edit Tugas</p>
        </header>

        
        <div class="row">
            <img src="<?php echo base_url().'template/assets/img/cek.jpg"'?> class="img-fluid" alt="" style="height: 600px;width: 540px">

           
              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                <div class="feature-box">
                  <form action="<?= base_url('index.php/C_dosen/simpan_edit_tugas') ?>" method="post" enctype="multipart/form-data">
                  <?php foreach ($tugas as $tugas) { ?>
                  <label>Judul</label>
                  <input type="hidden" class="form-control" name="matkul" value="<?= $tugas['id_matkul'] ?>">
                  <input type="hidden" class="form-control" name="id" value="<?= $tugas['id_tugas'] ?>">
                  <input type="hidden" class="form-control" name="kelas" value="<?= $tugas['id_kelas'] ?>">
                  <input type="text" class="form-control" name="judul" value="<?= $tugas['judul_tugas'] ?>">
                  <br>
                  <label>Deadline</label>
                  <input type="datetime-local" class="form-control" name="deadline" value="<?=date("d-M-Y H:i" , strtotime($tugas['deadline'])) ?>">
                  <small>(Deadline Sebelumnya : <?= date("d-M-Y\ H:i" , strtotime($tugas['deadline'])) ?>)</small>
                  <br><br>
                  <label>Deskripsi</label>
                  <textarea id="deskripsi" name="deskripsi" class="form-control w-100" cols="10" rows="4" placeholder=""><?= ($tugas['deskripsi_tugas']) ?></textarea>
                  <br>
                  <label>File</label>

                  <input type="file" class="form-control" name="file" value="">
                  <small>(File berbentuk doc, docx, ppt, pdf, rar, zip)</small><br>
                  <?php if($tugas['file_tugas'] != NULL ) { ?>
                  <small> (File Sebelumnya : <?= $tugas['file_tugas'] ?>)</small>
                  <?php } ?>
                  <br>
                  <input type="hidden" name="admin" value="<?= $this->session->userdata('id_akun') ?>">
                  <br>
                  <button type="submit" class="btn btn-primary"> Edit Tugas</button>
                  <?php } ?>
                  </form>
                </div>
              </div>
            

        </div>
  </div>
</section>

