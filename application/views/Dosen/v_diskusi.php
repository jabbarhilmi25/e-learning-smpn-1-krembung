<?php foreach($dis as $dis) { ?>
    <div class="form-control">

        <strong><?= $dis['nama_murid']?></strong> &nbsp &nbsp<i class="ri-calendar-todo-fill"></i> <?= date("d/m/Y H:i" , strtotime($dis['tgl_diskusi'])) ?> 

        <?php if($dis['poin_diskusi'] == '-5' ) { ?> &nbsp &nbsp <font color="red"><i class="ri-award-line"></i> <?= $dis['poin_diskusi']; ?> Poin</font> 
        <?php } else { ?>
            &nbsp &nbsp <font color="blue"><i class="ri-award-line"></i> <?= $dis['poin_diskusi']; ?> Poin
            <?php } ?>
        <?php } ?>
        <span style="float:right;">
        <?php if ($dis['id_akun'] == $akun['id_murid']) { ?>
            <a href="" class="edit" data-toggle="modal" data-target="#modal-edit" data-id="<?= $dis['id_diskusi']?>">
            <i class="ri-edit-box-line"> </i>Edit
            </a>&nbsp &nbsp 
            <a href="" class="hapus" data-toggle="modal" data-target="#modal-hapus" data-id="<?= $dis['id_diskusi']?>">
            <i class="ri-chat-delete-line"> </i>Hapus
            </a>
        <?php } ?>
            <a href="" class="report" data-id="<?= $dis['id_diskusi']?>" ><i class="ri-error-warning-line"></i> Report</a>
            </span> 
            <br>
            <?= nl2br($dis['isi_diskusi']) ?>
            </div>
            <input type="hidden" name="dis" value="<?= $dis['id_diskusi']; ?>">
            &nbsp <a href="" class="point10" data-id="<?= $dis['id_diskusi']?>"><i class="ri-thumb-up-line"></i> 10 Poin</a> &nbsp <a href="" class="point20" data-id="<?= $dis['id_diskusi']?>"><i class="ri-heart-line"></i> 20 Poin</a>
<?php } ?>