
<section id="services" class="services">

<div class="container" data-aos="fade-up">

  <header class="section-header">
  <br><br><br>
  </header>

  <div class="row gy-4">
   <?php
    foreach($tugas as $tugas) {
   ?>

    <div class="col-lg-12 col-md-1" data-aos="fade-up" data-aos-delay="200">
      <div class="service-box purple" style="padding: 20px 10px;">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $tugas['judul_tugas']?></h3>
        <p><?= $tugas['nama_matkul'] ?></p>
      </div>
    </div>

   <?= $this->session->flashdata('message'); ?>
    <div class="card" style="width: 100%;">
  <div class="card-body">
    <h4 class="card-title">Detail Tugas<span style="float:right;"><a href="<?= base_url('index.php/C_dosen/pengumpulan/'.$tugas['id_tugas']) ?>" class="btn btn-dark"><i class="ri-file-copy-2-line"></i> Pengumpulan</a> &nbsp &nbsp<a href="<?= base_url('index.php/C_dosen/kelas/'.$tugas['id_matkul']) ?>" class="btn btn-secondary"><i class="ri-arrow-left-line"></i> Kembali</a></span></h4>
  </div>
  <input type="hidden" id="id" value="<?= $id?>">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">
      <font color="#007aff"><p><i class="ri-calendar-event-fill"></i> Tanggal Dibuat : <?=date("d/m/Y H:i" , strtotime($tugas['tanggal_tugas'])) ?></p></font>
      <font color="red"><p><i class="ri-calendar-check-fill"></i> Deadline : <?=date("d/m/Y H:i" , strtotime($tugas['deadline'])) ?></p></font>
      <br>
      <p><?= nl2br($tugas['deskripsi_tugas']) ?></p><br>
      <?php if($tugas['file_tugas'] != NULL ) { ?>
      <div class="alert alert-secondary" role="alert"><strong><p>Download file dibawah ini !</p></strong>
      <a href="<?php echo base_url('index.php/C_dosen/download/'.$tugas['file_tugas'])?>" class="btn btn-dark btn-sm"><i class="ri-download-2-line"></i>  Download File</a> &nbsp <?= $tugas['file_tugas'] ?>
      <?php } ?>
      </div><br>
      <div class="form-control" style="background-color: #e0e0e0">
        <h4>Diskusi</h4>
        <div id="isi_diskusi">

        </div>
      </div>
      <br>
      <div class="form-group">
        <form id="form_1" method="POST">
          <textarea class="form-control" type="text" name="komen" id="komen" value="" placeholder="Tulis komentar..." required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
          oninput="this.setCustomValidity('')" ></textarea>
          <input class="form-control" type="hidden" name="id_tugas" value="<?= $tugas['id_tugas'] ?>">
          <button type="submit" class="btn btn-dark">Submit Komentar</button>
        </form>
      </div>
    </li>

    <?php } ?>
</div>

</div>

<!-- <script>
    document.onkeydown=function(evt){
        var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
        if(keyCode == 13)
        {
            //your function call here
            document.test.submit();
        }
    }
</script>  -->

</section>
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-dark">
        <h5 class="modal-title" id="exampleModalLabel">Edit Komentar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-update" method="post">
      <div class="modal-body" id="target_form_update">


      </div>
      <div class="modal-footer">
        <div class="form-group" align="right">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
          <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-dark">
        <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ingin menghapus komentar?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-delete" method="post">
      <div class="modal-body">
        <input type="hidden" id="id_hapus" name="id_hapus">
      </div>
      <div class="row">
        <div class="col-12">
          <div class="form-group" align="center">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 150px">Tidak</button>
            <button type="submit" class="btn btn-primary" style="width: 150px">Ya</button>
          </div>
        </div>
      </div>
      <br>
          </form>
    </div>
  </div>
</div>
