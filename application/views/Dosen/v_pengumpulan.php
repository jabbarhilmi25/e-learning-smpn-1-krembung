
<section id="services" class="services">

<div class="container" data-aos="fade-up">

<header class="section-header">
<br><br><br>
</header>

  <div class="row gy-4">
   <?php
    foreach($tugas as $tugas) {
   ?>

    <div class="col-lg-12 col-md-1" data-aos="fade-up" data-aos-delay="200">
      <div class="service-box purple" style="padding: 20px 10px;">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $tugas['judul_tugas']?></h3>
        <p><?= $tugas['nama_matkul'] ?></p>
      </div>
    </div>
    <?php } ?>

   <?= $this->session->flashdata('message'); ?>
    <div class="card" style="width: 100%;">
  <div class="card-body">
    <h4 class="card-title">Pengumpulan <span style="float:right;"><a href="<?= base_url('index.php/C_dosen/detail_matkul/'.$tugas['id_kelas'].$tugas['id_matkul']) ?>" class="btn btn-secondary"><i class="ri-arrow-left-line"></i> Kembali</a></span></h4>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">
    <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Tanggal</th>
            <th scope="col">File Jawaban</th>
            <th scope="col">Nilai</th>
            </tr>
        </thead>

        <?php
        $i = 0;
        foreach($jawab as $jawab) :
        $i++;
        ?>
        <tbody>
            <tr>
            <th scope="row"><?= $i; ?> </th>
            <td> <strong><?= $jawab['nama_murid'] ?></strong><br><small> <?= $jawab['id_murid'] ?> </small></td>
            <td> <?= date("d/m/Y H:i" , strtotime($jawab['tanggal_jawaban']) ) ?></td>
            <td><a href="<?php echo base_url('index.php/C_dosen/download/'.$jawab['file_jawaban'])?>"><i class="ri-file-text-line"></i>  <?= $tugas['file_tugas'] ?> </a></td>
            <td>
            <?php if($jawab['nilai_jawaban'] == NULL OR $jawab['nilai_jawaban'] == 0 ) { ?>
            <form action="<?= base_url('index.php/C_dosen/nilai/'.$jawab['id_tugas']) ?>" id="form-id" method="POST" >
            <input type="number" name="nilai" id="nilai" placeholder="Nilai" style="width:100px;" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
            oninput="this.setCustomValidity('')"  />
            <input type="hidden" name="id" id="id" value="<?= $jawab['id_jawaban'] ?>">
            <input type="hidden" name="tugas" id="tugas" value="<?= $jawab['id_tugas'] ?>">
            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit<?= $jawab['id_jawaban']?>">
             <i class="ri-checkbox-line"> </i> Beri Nilai
            </button>
            </form>
            <!-- <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#edit<?= $jawab['id_jawaban']?>">
             <i class="ri-checkbox-line"> </i>aaaaaaa
            </button> -->

            <?php } else { ?>
            <strong><?= $jawab['nilai_jawaban'] ?> <i class="ri-check-line"></i></strong>
            <?php } ?>
            </td>
            </tr>

        </tbody>
        <?php endforeach; ?>
        </table>
    </li>

</div>

</div>

</section><!-- End Services Section


            <!-- Modal -->
            <?php
            $no = 0;
            foreach($jwb as $jwb) :
            $no++;
            ?>
            <form action="<?= base_url('index.php/C_dosen/nilai/'.$jwb['id_tugas']) ?>" method="post">
            <div class="modal fade" id="edit<?= $jwb['id_jawaban']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header btn-dark">
                    <h5 class="modal-title" id="exampleModalLabel">Konfirmasi Nilai</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                  Apakah nilai yang Anda berikan sudah benar  ?
                  </div>
                  <div class="modal-footer btn-light">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                    <button type="button" class="btn btn-primary" onclick="document.getElementById('form-id').submit();">Ya</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
            <?php endforeach; ?>

<script type="text/javascript">
    $('#submit').click(function() {
    $.ajax({
        type: 'POST',
        data: {
            email: 'email@example.com',
            message: 'hello world!'
        },
        success: function(msg) {
            alert('Email Sent');
        }
    });
});
</script>
