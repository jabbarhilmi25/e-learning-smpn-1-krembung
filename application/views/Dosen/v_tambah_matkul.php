<section id="features" class="features">

  <br><br><br>
  <div class="container" data-aos="fade-up">

        <header class="section-header">
          <p>Tambah Mata Pelajaran</p>
        </header>


        <div class="row">
            <img src="<?php echo base_url().'template/assets/img/features-6.jpg"'?> class="img-fluid" alt="" style="height: 360px;width: 540px">


              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                <div class="feature-box">
                  <form action="<?= base_url('index.php/C_dosen/simpan_matkul') ?>" method="post">
                  <!-- <label>NIM/NIP</label>
                  <input type="text" class="form-control" name="id" value="">
                  <br> -->
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" value="" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
                            oninput="this.setCustomValidity('')"  />
                  <br>
                  <label>Jam Belajar</label>
                  <input type="text" class="form-control" name="jam" value="" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
                            oninput="this.setCustomValidity('')"  />
                  <br>
                  <input type="hidden" name="admin" value="<?= $this->session->userdata('id_akun') ?>">
                  <br>
                  <a href="<?= base_url('index.php/C_dosen/kelas') ?>" class="btn btn-secondary"> Kembali</a>
                  <button type="submit" class="btn btn-primary"> Tambah Mata Pelajaran</button>
                  </form>
                </div>
              </div>


        </div>
  </div>
</section>
