<!-- ======= Services Section ======= -->
<section id="services" class="services">

    <br><br><br>
<div class="container" data-aos="fade-up">

  <header class="section-header">

    <p>Detail Mata Pelajaran</p>
  </header>

  <div class="row gy-4">
   <?php
    foreach($mat as $matkul) {
   ?>

    <div class="col-lg-12 col-md-1" data-aos="fade-up" data-aos-delay="200">
      <div class="service-box blue" style="padding: 20px 10px;">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $matkul['nama_matkul']?></h3>
        <p><?= $matkul['id_matkul'] ?></p>
      </div>
    </div>

    <div class="card" style="width: 100%;">
  <div class="card-body">
    <h4 class="card-title">Daftar Tugas <span style="float:right;"><a href="<?= base_url('index.php/C_dosen/kelas') ?>" class="btn btn-secondary"><i class="ri-arrow-left-line"></i> Kembali</a></span></h4>
    <?php 
    $kelas = substr($id,0,5);
    ?>
    <a href="<?= base_url('index.php/C_dosen/tambah_tugas/'.$kelas.$matkul['id_matkul']) ?>" class="btn btn-primary" ><i class="ri-file-edit-line"></i> Tambah Tugas</a>
  </div>
  <?php } ?>
  <?= $this->session->flashdata('message'); ?>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">

        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">No</th>
            <th scope="col">Judul</th>
            <th scope="col">Tanggal</th>
            <th scope="col">Aksi</th>
            </tr>
        </thead>

        <?php
        $i = 0;
        foreach($tugas as $tugas) {
        $i++;
        ?>
        <tbody>
            <tr>
            <th scope="row"><?= $i; ?> </th>
            <td> <a href="<?= base_url('index.php/C_dosen/detail_tugas/'.$tugas['id_tugas']) ?>"> <?= $tugas['judul_tugas'] ?> </a></td>
            <td> <?=date("d/m/Y H:i" , strtotime($tugas['tanggal_tugas'])) ?></td>
            <td>
            <a href="<?= base_url('index.php/C_dosen/edit_tugas/'.$tugas['id_tugas']) ?>" class="btn btn-success"><i class="ri-edit-2-line"></i> Edit</a> &nbsp
            <a href="<?= base_url('index.php/C_dosen/pengumpulan/'.$tugas['id_tugas']) ?>" class="btn btn-dark"><i class="ri-file-copy-2-line"></i></i> Pengumpulan</a>
            </td>
            </tr>
        </tbody>
        <?php } ?>
        </table>
    </li>

</div>

</div>

</section><!-- End Services Section -->
