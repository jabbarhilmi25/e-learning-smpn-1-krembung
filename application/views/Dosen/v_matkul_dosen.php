<!-- ======= Services Section ======= -->
<section id="services" class="services">

<br><br><br>
<div class="container" data-aos="fade-up">

  <header class="section-header">
    <p>Daftar Mata Pelajaran</p>
  </header>
  <center>
    <a href="<?= base_url('index.php/C_dosen/tambah_matkul') ?>" class="btn btn-primary" ><i class="ri-folder-add-line"></i> Tambah Mata Pelajaran</a>&nbsp &nbsp
    <a href="<?= base_url('index.php/C_dosen/kelas') ?>" class="btn btn-primary" ><i class="ri-line-chart-fill"></i> Kembali</a><br><br>
    <?php foreach ($kelas as $kelas ) { ?>    
    <img src="<?= base_url().'uploadfile/'.$kelas['gambar']?>" style="height:100px; width:300px;" ><br>
    <?php } ?>
  </center>
  <br><br>
  <div class="row gy-4">
    <?= $this->session->flashdata('message'); ?> 
   <?php
    foreach($mat as $matkul) {
   ?>
    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
      <div class="service-box blue">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $matkul['nama_matkul']?></h3>
        <p><?= $matkul['id_matkul'] ?></p>
        <a href="<?= base_url('index.php/C_dosen/detail_matkul/'.$kelas['id_kelas'].$matkul['id_matkul']) ?>" class="read-more"><span>Detail</span><i class="ri-search-eye-line"></i></i>
        </a>
        <a href="<?= base_url('index.php/C_dosen/edit_matkul/'.$matkul['id_matkul']) ?>" class="read-more"><span>Edit</span><i class="ri-edit-2-line"></i>
        </a>
      </div>
    </div>

  
   <?php } ?>
</div>

</section><!-- End Services Section -->