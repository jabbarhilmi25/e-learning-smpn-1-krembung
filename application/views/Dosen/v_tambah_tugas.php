<section id="features" class="features">

  <br><br><br>
  <div class="container" data-aos="fade-up">
        <?php $data=$this->M_matkul->get_detail($id_matkul);?>
        <header class="section-header">
          <p>Tambah Tugas</p>          
          <?php foreach($data as $data){
            echo $data['nama_matkul'];
          } ?>
        </header>

        
        <div class="row">
            <img src="<?php echo base_url().'template/assets/img/code.jpg"'?> class="img-fluid" alt="" style="height: 600px;width: 540px">

           
              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                <div class="feature-box">
                  <form action="<?= base_url('index.php/C_dosen/simpan_tugas') ?>" method="post" enctype="multipart/form-data">
                  <input type="hidden" name="matkul" value="<?=substr($id,5) ?>">
                  <input type="hidden" name="id" value="<?= $id ?>">
                  <input type="hidden" name="kelas" value="<?= substr($id,0,5) ?>">

                  <label>Judul</label>
                  <input type="text" class="form-control" name="judul" value="" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
                            oninput="this.setCustomValidity('')" >
                  <br>
                  <label>Deadline</label>
                  <input type="datetime-local" class="form-control" name="deadline" value="" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
                            oninput="this.setCustomValidity('')" >
                  <br>
                  <label>Deskripsi</label>
                  <textarea id="deskripsi" name="deskripsi" class="form-control w-100" cols="10" rows="4" placeholder="" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
                            oninput="this.setCustomValidity('')" ></textarea>
                  <br>
                  <label>File</label>
                  <input type="file" class="form-control" name="file" value="">
                  <small>(File berbentuk doc, docx, ppt, pdf, rar, zip)</small>
                  <br>
                  <input type="hidden" name="admin" value="<?= $this->session->userdata('id_akun') ?>">
                  <br>
                  <button type="submit" class="btn btn-primary"> Tambah Tugas</button>
                  </form>
                </div>
              </div>
            

        </div>
  </div>
</section>

