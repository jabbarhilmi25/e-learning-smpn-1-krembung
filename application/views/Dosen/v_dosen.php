

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="hero d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center">
          <h1 data-aos="fade-up">Selamat Datang, <?php echo $akun['nama_guru']; ?></h1>
          <h2 data-aos="fade-up" data-aos-delay="400">Cyber School SMPN 1 Krembung</h2>
          <h3></h3>
          <div data-aos="fade-up" data-aos-delay="600">
            <div class="text-center text-lg-start">
              <?php if(md5($akun['id_guru']) == $akun['password_guru']) { ?>
              <div class="alert alert-danger" role="alert"> <i class="ri-lock-unlock-line"></i> Password masih setelan default, Silahkan ganti password anda !
                <br><a href="<?= base_url('index.php/C_dosen/ganti') ?>">Ganti Passsword <i class="bi bi-arrow-right"></i></a>
              </div>
              <?php } ?>
              <?= $this->session->flashdata('message'); ?>
              <a href="#values" class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
                <span>Mulai</span>
                <i class="bi bi-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
          <img src="<?php echo base_url().'template/assets/img/features-2.png"'?> class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">
    

    <section id="values" class="values">

      <div class="container" data-aos="fade-up">

        <div class="row" style=" justify-content: center;">

          <div class="col-lg-4">
            <div class="box" data-aos="fade-up" data-aos-delay="200">
              <a href="<?= base_url('index.php/C_dosen/kelas') ?>">
              <img src="<?php echo base_url().'template/assets/img/mat-1.jpg'?>" class="img-fluid" alt="" style="height: 250px">
              <h3>Kelas</h3></a>
              <p>Mengelola kelas, mata pelajaran, dan menambahkan tugas</p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="box" data-aos="fade-up" data-aos-delay="400">
            <a href="<?= base_url('index.php/C_dosen/show_nilai') ?>">
              <img src="<?php echo base_url().'template/assets/img/mat-2.jpg'?>" class="img-fluid" alt="" style="height: 250px">
              <h3>Nilai</h3></a>
              <p>Mengelola nilai siswa</p>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="box" data-aos="fade-up" data-aos-delay="600">
              <a href="<?= base_url('index.php/C_dosen/ganti') ?>">
              <img src="<?php echo base_url().'template/assets/img/mat-3.jpg'?>" class="img-fluid" alt="" style="height: 250px">
              <h3>Ganti Password</h3></a>
              <p>Mengganti password akun</p>              
            </div>
          </div>

        </div>

      </div>

    </section><!-- End Values Section -->