

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="hero d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center">
          <h1 data-aos="fade-up">Selamat Datang di </h1>
          <h1 data-aos="fade-up">Cyber School SMPN 1 Krembung</h1>
          <h2 data-aos="fade-up" data-aos-delay="400">Dari Sini Pencerahan Bersemi</h2>
          <div data-aos="fade-up" data-aos-delay="600">
            <div class="text-center text-lg-start">
              <a href="<?= base_url('index.php/C_login') ?>" class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
                <span><i class="ri-clockwise-2-line"></i> Login</span>
                <i class="bi bi-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
          <img src="<?php echo base_url().'template/assets/img/hero-img.png"'?> class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  </main><!-- End #main -->

  <!-- ======= About Section ======= -->
  <!-- <section id="about" class="about">

      <div class="container" data-aos="fade-up">
        <div class="row gx-0">

          <div class="col-lg-6 d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="200">
            <div class="content">
              <h3>UMSIDA</h3>
              <h2>Menjadi perguruan tinggi unggul dan inovatif dalam pengembangan IPTEKS berdasarkan nilai-nilai Islam untuk kesejahteraan masyarakat.</h2>
              <p>
                Universitas Muhammadiyah Sidoarjo (selanjutnya disebut UMSIDA) dengan semboyannya “Dari Sini Pencerahan Bersemi” termasuk salah satu universitas swasta di Jawa Timur yang berkomitmen menjadi salah satu perguruan tinggi unggul dan inovatif dalam pengembangan IPTEKS berdasarkan nilai-nilai Islam untuk kesejahteraan masyarakat di mana UMSIDA menjadi pusat pencerahan melalui pengembangan sumber daya manusia yang berorientasi pada iman, ilmu, dan amal serta pengembangan tradisi intelektual untuk mewujudkan masyarakat yang berkemajuan.
              </p>
              <div class="text-center text-lg-start">
                <a href="https://umsida.ac.id/" class="btn-read-more d-inline-flex align-items-center justify-content-center align-self-center" target="blank">
                  <span>Baca Lebih Lanjut</span>
                  <i class="bi bi-arrow-right"></i>
                </a>
              </div>
            </div>
          </div>

          <div class="col-lg-6 d-flex align-items-center" data-aos="zoom-out" data-aos-delay="200">
            <img src="<?php echo base_url().'template/assets/img/about.jpg"'?> class="img-fluid" alt="">
          </div>

        </div>
      </div>

    </section>End About Section -->