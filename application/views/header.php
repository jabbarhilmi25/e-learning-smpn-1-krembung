<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Cyber school</title>
  <meta content="" name="description">

  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url('template/assets/img/logoSmp.png')?>" rel="icon">
  <link href="<?= base_url('template/assets/img/apple-touch-icon2.png')?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?= base_url('template/assets/vendor/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
  <link href="<?= base_url('template/assets/vendor/bootstrap-icons/bootstrap-icons.css')?>" rel="stylesheet">
  <link href="<?= base_url('template/assets/vendor/aos/aos.css')?>" rel="stylesheet">
  <link href="<?= base_url('template/assets/vendor/remixicon/remixicon.css')?>" rel="stylesheet">
  <link href="<?= base_url('template/assets/vendor/swiper/swiper-bundle.min.css')?>" rel="stylesheet">
  <link href="<?= base_url('template/assets/vendor/glightbox/css/glightbox.min.css')?>" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url('template/assets/css/style.css')?>" rel="stylesheet">

  <!-- =======================================================
  * Template Name: FlexStart - v1.2.0
  * Template URL: https://bootstrapmade.com/flexstart-bootstrap-startup-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top">
    <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

      <nav class="navbar">
      <!-- <a href="<?= base_url('template/index.html')?>" class="logo d-flex align-items-center">
        <img src="<?= base_url('template/assets/img/logoSmp.png')?>" alt="">
        <span>FlexStart</span>
      </a> -->
      
        <img src="<?= base_url('template/assets/img/logoSmp.png')?>" style="height: 100px;width: 100px">
          
      </nav>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="getstarted scrollto" href="<?= base_url('index.php/C_login') ?>"> <i class="ri-clockwise-2-line"></i>&nbsp Login</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->