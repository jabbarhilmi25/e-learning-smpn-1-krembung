
    <section id="values" class="values">

      <div class="container" data-aos="fade-up">

        <div class="row" style=" justify-content: center;">

          <div class="col-lg-4">
            <div class="box" data-aos-delay="0">
              <a href="<?= base_url('index.php/C_login/v_loginGuru') ?>">
              <img src="<?php echo base_url().'template/assets/img/guru.jpg"'?>" class="img-fluid" alt="">
              <h3>Guru</h3></a>
              <p>Login Sebagai Guru</p>
            </div>
          </div>

          <div class="col-lg-4 mt-4 mt-lg-0">
            <div class="box" data-aos-delay="0">
            <a href="<?= base_url('index.php/C_login/v_loginSiswa') ?>">
              <img src="<?php echo base_url().'template/assets/img/murid.jpg"'?>" class="img-fluid" alt="">
              <h3>Siswa</h3></a>
              <p>Login Sebagai Siswa</p>
            </div>
          </div>

          

        </div>

      </div>

    </section><!-- End Values Section -->