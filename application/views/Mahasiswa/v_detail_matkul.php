<!-- ======= Services Section ======= -->
<section id="services" class="services">

<div class="container" data-aos="fade-up">

  <header class="section-header">

    <p>Detail Mata Pelajaran</p>
  </header>

  <div class="row gy-4">
   <?php
    foreach($mat as $matkul) {
   ?>

    <div class="col-lg-12 col-md-1" data-aos="fade-up" data-aos-delay="200">
      <div class="service-box blue" style="padding: 20px 10px;">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $matkul['nama_matkul']?></h3>
        <p><?= $matkul['id_matkul'] ?></p>
      </div>
    </div>

   <?php } ?>

    <div class="card" style="width: 100%;">
  <div class="card-body">
    <h4 class="card-title">Daftar Tugas <span style="float:right;"><a href="<?= base_url('index.php/C_mhs/kelas') ?>" class="btn btn-secondary"><i class="ri-arrow-left-line"></i> Kembali</a></span></h4>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">

        <table class="table">
        <thead class="thead-dark">
            <tr>
            <th scope="col">No</th>
            <th scope="col">Judul</th>
            <th scope="col">Tanggal</th>
            </tr>
        </thead>

        <?php
        $i = 0;
        foreach($tugas as $tugas) {
        $i++;
        ?>
        <tbody>
            <tr>
            <th scope="row"><?= $i; ?> </th>
            <td> <a href="<?= base_url('index.php/C_mhs/detail_tugas/'.$tugas['id_tugas']) ?>"> <?= $tugas['judul_tugas'] ?> </a></td>
            <td>  <?=date("d/m/Y H:i" , strtotime($tugas['tanggal_tugas'])) ?></td>
            </tr>
        </tbody>
        <?php } ?>
        </table>
    </li>

</div>

</div>

</section><!-- End Services Section -->
