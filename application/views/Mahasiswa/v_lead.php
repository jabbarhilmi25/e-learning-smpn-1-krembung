<!-- ======= Services Section ======= -->
<section id="services" class="services">
<
<div class="container" data-aos="fade-up">

  <header class="section-header">

    <p>Leaderboard</p>
  </header>
  <center>
   
    <a href="<?= base_url('index.php/C_mhs/kelas') ?>" class="btn btn-primary" ><i class="ri-folders-fill"></i> Daftar Kelas</a>&nbsp &nbsp
  </center>
  <br><br>
  <div class="row gy-4">
    <?= $this->session->flashdata('message'); ?> 
   <?php
    foreach($mat as $matkul) {
   ?>
    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
      <div class="service-box orange">
        <i class="ri-star-line icon"></i>
        <h3><?= $matkul['nama_matkul']?></h3>
        <p><?= $matkul['id_matkul'] ?></p>

        <table class="table">
        <?php 
            $lead = $this->M_lead->lead($matkul['id_matkul']);
             $i = 0;
            if($lead == NULL ) { echo "<strong>Data Leaderboard Kosong !</strong>";
            } else { 
            foreach ($lead as $lead) 
            { $i++; ?>
          <tbody>
            
            <tbody>
            <tr>
            <th scope="row"><?= $i; ?></th>
            <td style="text-align: left"><i class="ri-star-smile-line"></i> <?= floatval($lead['hasil'] )?></td>
            <td  style="text-align: left"><i class="ri-user-line"></i></i> <?= $lead['nama_murid'] ?></td>
            </tr>
          </tbody>
          <?php }?>
        <?php } ?>
        </table>
        
        <!-- <a href="<?= base_url('index.php/C_dosen/detail_matkul/'.$matkul['id_matkul']) ?>" class="read-more"><span>Detail</span><i class="ri-search-eye-line"></i></i>
        </a>
        <a href="<?= base_url('index.php/C_dosen/edit_matkul/'.$matkul['id_matkul']) ?>" class="read-more"><span>Edit</span><i class="ri-edit-2-line"></i>
        </a> -->
      </div>
    </div>

  
   <?php } ?>
      <div class="alert alert-warning" role="alert">
      <h6><i class="ri-star-smile-line"></i> Perolehan poin = Rata-rata nilai tugas + rata-rata diskusi tugas</h6>
      </div>
   
</div>

</section><!-- End Services Section -->

<script type="text/javascript">
  function refreshPage(){
    window.location.reload();
} </script>