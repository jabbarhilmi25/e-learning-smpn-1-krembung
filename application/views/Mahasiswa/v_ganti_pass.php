<section id="contact" class="contact">

      <br><br><br>
      <div class="container" data-aos="fade-up">

        <header class="section-header">
          <p>Ganti Password</p>
          <?= $this->session->flashdata('message'); ?>
        </header>

        <div class="row gy-4">

          <center>
          <div class="col-lg-6">

            <form action="<?= base_url('index.php/C_mhs/simpan_pass') ?>" method="post">
              <div class="row gy-4">

                <div class="col-md-6">
                  <input type="password" name="pass1" class="form-control" placeholder="Password Baru" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
                            oninput="this.setCustomValidity('')" >
                </div>

                <div class="col-md-6 ">
                  <input type="password" class="form-control" name="pass2" placeholder="Ulangi Password Baru" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
                            oninput="this.setCustomValidity('')" >
                  <font size="1" color="red">*Password harus terdiri dari 8-16 karakter</font><br>
                  
                </div>
                <center>
                  <button class="btn btn-primary" type="submit">Ganti Password</button>
                </center>

              </div>
            </form>

          </div>
          </center>

        </div>

      </div>

    </section><!-- End Contact Section -->