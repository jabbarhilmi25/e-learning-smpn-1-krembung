
<section id="services" class="services">

<div class="container" data-aos="fade-up">

  <header class="section-header">
  <br><br><br>
  </header>

  <div class="row gy-4">
   <?php
    foreach($tugas as $tugas) {
   ?>

    <div class="col-lg-12 col-md-1" data-aos="fade-up" data-aos-delay="200">
      <div class="service-box purple" style="padding: 20px 10px;">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $tugas['judul_tugas']?></h3>
        <p><?= $tugas['nama_matkul'] ?></p>
      </div>
    </div>

   <?= $this->session->flashdata('message'); ?>
    <div class="card" style="width: 100%;">
  <div class="card-body">
    <h4 class="card-title">Detail Tugas <span style="float:right;"><a href="<?= base_url('index.php/C_mhs/detail_matkul/'.$tugas['id_kelas'].$tugas['id_matkul']) ?>" class="btn btn-secondary"><i class="ri-arrow-left-line"></i> Kembali</a></span></h4>
  </div>
  <input type="hidden" id="id" value="<?= $id?>">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">
      <font color="#007aff"><p><i class="ri-calendar-event-fill"></i> Tanggal Dibuat : <?=date("d/m/Y H:i" , strtotime($tugas['tanggal_tugas'])) ?></p></font>
      <font color="red"><p><i class="ri-calendar-check-fill"></i> Deadline : <?=date("d/m/Y H:i" , strtotime($tugas['deadline'])) ?></p></font>
      <br>
      <p><?= nl2br($tugas['deskripsi_tugas']) ?></p><br>

      <?php if($tugas['file_tugas'] != NULL ) { ?>
      <div class="alert alert-secondary" role="alert"><strong><p>Download file dibawah ini !</p></strong>
      <a href="<?php echo base_url('index.php/C_dosen/download/'.$tugas['file_tugas'])?>" class="btn btn-dark btn-sm"><i class="ri-download-2-line"></i>  Download File</a> &nbsp <?= $tugas['file_tugas'] ?>
      <?php } ?>
      </div><br>

      <div class="form-control" style=" background-color:#343a40;">
      <font color="white"><strong><p>Upload file jawaban disini !</p></strong>

      <?php $jawaban = $this->M_matkul->getjawaban_byakun($tugas['id_tugas'],$akun['id_murid']); ?>

        <?php if($tugas['deadline'] <= date("Y-m-d H:i:s") ) { ?>
            <div class="alert alert-warning" role="alert"><strong><p>Pengumpulan tugas telah melebihi deadline !</p></strong>
            <?php if ($jawaban == true) { ?>
              <?php foreach ($jawaban as $jawaban) { ?>
                <div class="alert alert-success" role="alert"><i class="ri-checkbox-line"></i> File yang telah Anda upload : <a href="<?php echo base_url('index.php/C_mhs/download/'.$jawaban['file_jawaban'])?>"><i class="ri-file-3-line"></i>  <?= $jawaban['file_jawaban'] ?> </a> </div>
              <?php } ?>
            <?php } else { ?>
                <div class="alert alert-danger" role="alert"><i class="ri-alert-line"></i> Anda tidak mengumpulkan jawaban tugas !</div>
            <?php } //end if ?>
            </div>
        <?php } else { ?>
              <?php if ($jawaban == true) { ?>
              <?php foreach ($jawaban as $jawaban) { ?>
              <div class="alert alert-secondary" role="alert"><i class="ri-checkbox-line"></i> File yang telah di upload : <a href="<?php echo base_url('index.php/C_mhs/download/'.$jawaban['file_jawaban'])?>"><i class="ri-file-3-line"></i>  <?= $jawaban['file_jawaban'] ?> </a> </div>
              <?php } ?>
              <?php } else { ?>

              <form action="<?= base_url('index.php/C_mhs/upload_jawaban/'.$tugas['id_tugas']) ?>" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?= $tugas['id_tugas']?>">
                <input type="hidden" name="murid" value="<?= $akun['id_murid']; ?>">
                <input type="hidden" name="matkul" value="<?= $tugas['id_matkul']; ?>">

                <div class="input-group mb-3">
                  <input type="file" class="form-control" name="file" aria-label="Recipient's username" aria-describedby="basic-addon2" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
                  oninput="this.setCustomValidity('')"  />
                  <div class="input-group-append">
                    <button type="submit" class="btn btn-success btn-sm" style="height:39px"><i class="ri-upload-2-line"></i>  Upload File</button>
                  </div>
                </div>
                <small><i class="ri-file-3-line"></i> File harus berbentuk doc, docx, ppt, pdf, rar, zip</small>
                </font>
              </form>
              <?php } //end if ?>
        <?php } //end if ?>
      </div><br>

      <div id="comments-container" class="form-control" style="background-color: #e0e0e0" >
        <h4>Diskusi</h4>
          <div id="isi_diskusi">

          </div>
        <br>
      </div>
      <div class="form-grup">
        <form id="form_1" method="POST">
          <textarea id="komen" class="form-control" type="text" name="komen" placeholder="Tulis komentar..." required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')"
          oninput="this.setCustomValidity('')" ></textarea>
          <input type="hidden" name="id_tugas" id="id_tugas" value="<?= $tugas['id_tugas']?>">
          <button type="submit" class="btn btn-dark">Submit Komentar</button>
        </form>
      </div>
    </li>

    <?php } ?>
</div>

</div>


 <!-- Modal -->

<!-- <script>
    document.onkeydown=function(evt){
        var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
        if(keyCode == 13)
        {
            //your function call here
            document.test.submit();
        }
    }
</script>  -->

</section>
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-dark">
        <h5 class="modal-title" id="exampleModalLabel">Edit Komentar</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-update" method="post">
      <div class="modal-body" id="target_form_update">


      </div>
      <div class="modal-footer">
        <div class="form-group" align="right">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
          <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-dark">
        <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin ingin menghapus komentar?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="form-delete" method="post">
      <div class="modal-body">
        <input type="hidden" id="id_hapus" name="id_hapus">
      </div>
      <div class="row">
        <div class="col-12">
          <div class="form-group" align="center">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="width: 150px">Tidak</button>
            <button type="submit" class="btn btn-primary" style="width: 150px">Ya</button>
          </div>
        </div>
      </div>
      <br>
          </form>
    </div>
  </div>
</div>

    <!--



<script type="text/javascript">

//   var commentsArray = [{
//    "id": 1,
//    "parent": null,
//    "created": document.getElementById("tgl");,
//    "modified": document.getElementById("tgl");,
//    "content": document.getElementById("komen");,
//    "pings": [],
//    "creator": 6,
//    "fullname": document.getElementById("akun");,
//    "profile_picture_url": "https://viima-app.s3.amazonaws.com/media/public/defaults/user-icon.png",
//    "created_by_admin": false,
//    "created_by_current_user": false,
//    "upvote_count": 3,
//    "user_has_upvoted": false,
//    "is_new": false
// },
// // ...
// ]

// var usersArray = [{
//     id: 1,
//     fullname: document.getElementById("akun"),
//     email: "current.user@viima.com",
//     profile_picture_url: "https://viima-app.s3.amazonaws.com/media/public/defaults/user-icon.png"
//    },
//    // ...
// ]
// var saveComment = function(data) {

//   // Convert pings to human readable format
//   $(data.pings).each(function(index, id) {
//     var user = usersArray.filter(function(user){return user.id == id})[0];
//     data.content = data.content.replace('@' + id, '@' + user.fullname);
//   });

//   return data;
// }

// $('#comments-container').comments({
//   profilePictureURL: 'https://viima-app.s3.amazonaws.com/media/public/defaults/user-icon.png',
//   currentUserId: 1,
//   roundProfilePictures: true,
//   textareaRows: 1,
//   enableAttachments: true,
//   enableHashtags: true,
//   enablePinging: true,
//   getUsers: function(success, error) {
//     setTimeout(function() {
//       success(usersArray);
//     }, 500);
//   },
//   getComments: function(success, error) {
//     setTimeout(function() {
//       success(commentsArray);
//     }, 500);
//   },
//   postComment: function(data, success, error) {
//     setTimeout(function() {
//       success(saveComment(data));
//     }, 500);
//   },
//   putComment: function(data, success, error) {
//     setTimeout(function() {
//       success(saveComment(data));
//     }, 500);
//   },
//   deleteComment: function(data, success, error) {
//     setTimeout(function() {
//       success();
//     }, 500);
//   },
//   upvoteComment: function(data, success, error) {
//     setTimeout(function() {
//       success(data);
//     }, 500);
//   },
//   uploadAttachments: function(dataArray, success, error) {
//     setTimeout(function() {
//       success(dataArray);
//     }, 500);
//   },
// });

// $('#comments-container').comments({
//   // User
//   profilePictureURL: '',
//   currentUserIsAdmin: false,
//   currentUserId: null,

//   // Font awesome icon overrides
//   spinnerIconURL: '',
//   upvoteIconURL: '',
//   replyIconURL: '',
//   uploadIconURL: '',
//   attachmentIconURL: '',
//   fileIconURL: '',
//   noCommentsIconURL: '',

//   // Strings to be formatted (for example localization)
//   textareaPlaceholderText: 'Add a comment',
//   newestText: 'Newest',
//   oldestText: 'Oldest',
//   popularText: 'Popular',
//   attachmentsText: 'Attachments',
//   sendText: 'Send',
//   replyText: 'Reply',
//   editText: 'Edit',
//   editedText: 'Edited',
//   youText: 'You',
//   saveText: 'Save',
//   deleteText: 'Delete',
//   newText: 'New',
//   viewAllRepliesText: 'View all __replyCount__ replies',
//   hideRepliesText: 'Hide replies',
//   noCommentsText: 'No comments',
//   noAttachmentsText: 'No attachments',
//   attachmentDropText: 'Drop files here',
//   textFormatter: function(text) {return text},

//   // Functionalities
//   enableReplying: true,
//   enableEditing: true,
//   enableUpvoting: true,
//   enableDeleting: true,
//   enableAttachments: false,
//   enableHashtags: false,
//   enablePinging: false,
//   enableDeletingCommentWithReplies: false,
//   enable<a href="https://www.jqueryscript.net/tags.php?/Navigation/">Navigation</a>: true,
//   postCommentOnEnter: false,
//   forceResponsive: false,
//   readOnly: false,
//   defaultNavigationSortKey: 'newest',

//   // Colors
//   highlightColor: '#2793e6',
//   deleteButtonColor: '#C9302C',

//   scrollContainer: this.$el,
//   roundProfilePictures: false,
//   textareaRows: 2,
//   textareaRowsOnFocus: 2,
//   textareaMaxRows: 5,
//   maxRepliesVisible: 2,

//   fieldMappings: {
//     id: 'id',
//     parent: 'parent',
//     created: 'created',
//     modified: 'modified',
//     content: 'content',
//     file: 'file',
//     fileURL: 'file_url',
//     fileMimeType: 'file_mime_type',
//     pings: 'pings',
//     creator: 'creator',
//     fullname: 'fullname',
//     profileURL: 'profile_url',
//     profilePictureURL: 'profile_picture_url',
//     isNew: 'is_new',
//     createdByAdmin: 'created_by_admin',
//     createdByCurrentUser: 'created_by_current_user',
//     upvoteCount: 'upvote_count',
//     userHasUpvoted: 'user_has_upvoted'
//   },
// });
</script>

<!-- test -->
