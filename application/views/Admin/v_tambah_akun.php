<section id="features" class="features">

  <br><br><br>
  <div class="container" data-aos="fade-up">

        <header class="section-header">
          <p>Tambah Akun</p>
        </header>

        
        <div class="row">
            <img src="<?php echo base_url().'template/assets/img/features-4.jpg"'?> class="img-fluid" alt="" style="height: 360px;width: 540px">

           
              <div class="col-md-6" data-aos="zoom-out" data-aos-delay="200">
                <div class="feature-box">
                  <form action="<?= base_url('index.php/C_admin/simpan_tambah') ?>" method="post">
                  <label>NIM/NIP</label>
                  <input type="text" class="form-control" name="id" value="">
                  <br>
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama" value="">
                  <br>
                  <label>Level</label><br>
                  <select class="form-select" aria-label="Default select example" name="lvl">
                    <option selected>Pilih Level</option> 
                    <option value="1">Admin</option>                
                    <option value="2">Dosen</option>
                    <option value="3">Mahasiswa</option>
                  </select>
                  <input type="hidden" name="admin" value="<?= $this->session->userdata('id_akun') ?>">
                  <br>
                  <a href="<?= base_url('index.php/C_admin/akun') ?>" class="btn btn-secondary"> Kembali</a>
                  <button type="submit" class="btn btn-primary"> Tambah Akun</button>
                  </form>
                </div>
              </div>
            

        </div>
  </div>
</section>

