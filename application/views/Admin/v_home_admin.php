

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="hero d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center">
          <h1 data-aos="fade-up">Selamat Datang , <?= $akun['nama_admin'] ?></h1>
          <h2 data-aos="fade-up" data-aos-delay="400">Admin E-Learning UMSIDA</h2>
          <div data-aos="fade-up" data-aos-delay="600">
            <div class="text-center text-lg-start">
              <?php if(md5($akun['id_admin']) == $akun['password_admin']) { ?>
              <div class="alert alert-danger" role="alert"> <i class="ri-lock-unlock-line"></i> Password masih setelan default, Silahkan ganti password anda !
                <br><a href="<?= base_url('index.php/C_mhs/ganti') ?>">Ganti Passsword <i class="bi bi-arrow-right"></i></a>
              </div>
              <?php } ?>
              <?= $this->session->flashdata('message'); ?>
              <a href="#values" class="btn-get-started scrollto d-inline-flex align-items-center justify-content-center align-self-center">
                <span>Mulai</span>
                <i class="bi bi-arrow-right"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-lg-6 hero-img" data-aos="zoom-out" data-aos-delay="200">
          <img src="<?php echo base_url().'template/assets/img/features-4.png"'?> class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= About Section ======= -->
    <!-- <section id="about" class="about">

      <div class="container" data-aos="fade-up">
        <div class="row gx-0">

          <div class="col-lg-6 d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="200">
            <div class="content">
              <h3>Who We Are</h3>
              <h2>Expedita voluptas omnis cupiditate totam eveniet nobis sint iste. Dolores est repellat corrupti reprehenderit.</h2>
              <p>
                Quisquam vel ut sint cum eos hic dolores aperiam. Sed deserunt et. Inventore et et dolor consequatur itaque ut voluptate sed et. Magnam nam ipsum tenetur suscipit voluptatum nam et est corrupti.
              </p>
              <div class="text-center text-lg-start">
                <a href="#" class="btn-read-more d-inline-flex align-items-center justify-content-center align-self-center">
                  <span>Read More</span>
                  <i class="bi bi-arrow-right"></i>
                </a>
              </div>
            </div>
          </div>

          <div class="col-lg-6 d-flex align-items-center" data-aos="zoom-out" data-aos-delay="200">
            <img src="<?php echo base_url().'template/assets/img/about.jpg"'?> class="img-fluid" alt="">
          </div>

        </div>
      </div>

    </section>End About Section -->

    <section id="values" class="values">

      <div class="container" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-3">
            <div class="box" data-aos="fade-up" data-aos-delay="200">
              <a href="<?= base_url('index.php/C_admin/kelas') ?>">
              <img src="<?php echo base_url().'template/assets/img/values-1.png"'?> class="img-fluid" alt="">
              <h3>Kelas</h3>
              </a>
              <p>Mengelola mata kuliah dan menambahkan tugas</p>
            </div>
          </div>

          <div class="col-lg-3 mt-4 mt-lg-0">
            <div class="box" data-aos="fade-up" data-aos-delay="400">
            <a href="<?= base_url('index.php/C_admin/show_nilai') ?>">
              <img src="<?php echo base_url().'template/assets/img/values-3.png"'?> class="img-fluid" alt="">
              <h3>Nilai</h3>
              </a>
              <p>Mengelola nilai mahasiswa</p>
            </div>
          </div>

          <div class="col-lg-3 mt-4 mt-lg-0">
            <div class="box" data-aos="fade-up" data-aos-delay="600">
              <a href="<?= base_url('index.php/C_admin/akun') ?>">
              <img src="<?php echo base_url().'template/assets/img/values-2.png"'?> class="img-fluid" alt="">
              <h3>Manajemen Akun</h3>
              </a>
              <p>Mengelola akun dosen dan mahasiswa</p>
            </div>
          </div>

          <div class="col-lg-3 mt-4 mt-lg-0">
            <div class="box" data-aos="fade-up" data-aos-delay="600">
              <a href="<?= base_url('index.php/C_admin/ganti') ?>">
              <img src="<?php echo base_url().'template/assets/img/values-4.png"'?> class="img-fluid" alt="">
              <h3>Ganti Password</h3></a>
              <p>Mengganti password akun</p>
            </div>
          </div>

        </div>

      </div>

    </section><!-- End Values Section -->