<!-- ======= Services Section ======= -->
<section id="services" class="services">

<div class="container" data-aos="fade-up">

  <header class="section-header">
    
    <p>Detail Mata Kuliah</p>
  </header>
  <div class="row gy-4">
   <?php
    foreach($mat as $matkul) {
   ?>

    <div class="col-lg-12 col-md-1" data-aos="fade-up" data-aos-delay="200">
      <div class="service-box red" style="padding: 20px 10px;">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $matkul['nama_matkul']?></h3>
        <p><?= $matkul['id_matkul'] ?></p>
      </div>
    </div>
        
   <?php } ?>


    <div class="card" style="width: 100%;">
  <div class="card-body">
    <<h4 class="card-title">Daftar Nilai <span style="float:right;">
      <form action="<?php echo base_url('index.php/C_admin/cari_nilai/'.$matkul['id_matkul'])  ?>" class="form-control btn-light" method="get">
          <input type="search" name="cari" id="cari" placeholder="Cari Nama...">&nbsp &nbsp
          <button type="submit" class="btn btn-light"><i class="ri-search-2-line"></i> Cari</button>&nbsp &nbsp
          <a href="<?= base_url('index.php/C_admin/show_nilai') ?>" class="btn btn-secondary"><i class="ri-arrow-left-line"></i> Kembali</a>
      </form>
     </span>
    </h4>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">
      <?php if ($tugas==NULL) { ?>
       <h4>Tidak ada data</h4>
      <?php } else { ?>
        <table class="table table-borderless">
        <thead class="thead-dark">
            <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <?php foreach($tugas as $tugas) { ?>
            <th scope="col"> <?= $tugas['judul_tugas']?></th>
            <?php } ?>
            <th scope="col">Rata-rata</th>      
            </tr>
        </thead>
        
        <?php 
        $i = 0;
        // $tgs=$this->M_matkul->get_tugas($matkul['id_matkul']);
        foreach($acc as $acc) {
        $i++;
        ?>
        <tbody>
            <tr>
            <th scope="row"><?= $i; ?> </th>
            <td>
              <strong><?= $acc['nama_murid'] ?></strong><br>
              <small><?= $acc['id_murid'] ?></small>
            </td>
            </td>
            <?php 
              $nilai=$this->M_matkul->get_nilai($matkul['id_matkul'],$acc['id_murid']);
              foreach ($nilai as $nilai) { ?>
              <td> 
              <?php
              if($nilai['nilai_jawaban'] != 0){
                echo $nilai['nilai_jawaban'];
                
              }

              elseif($nilai['nilai_jawaban']==0){
                echo "Tugas belum dikumpulkan";
              }

              elseif($nilai== NULL) {
                echo "Tidak ada data";
              }
              ?> 
              </td>               
            <?php } ?> 
            <td>
            <?php 
              $rata=$this->M_matkul->get_rata($matkul['id_matkul'],$acc['id_murid']);
              foreach ($rata as $rata) {
                if($rata['rata'] != 0){
                  
                  echo floatval($rata['rata']);
                }
                else{
                  echo "0";
                }
                
              }              
            ?>
            </td>          
            </tr>
        </tbody>
        <?php } ?>
        </table>
      <?php } //end if ?>
    </li>

</div>

</div>

</section><!-- End Services Section -->