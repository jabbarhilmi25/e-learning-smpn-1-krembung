<!-- ======= Services Section ======= -->
<section id="services" class="services">

<br><br><br>
<div class="container" data-aos="fade-up">

  <header class="section-header">
    <p>Daftar Mata Pelajaran</p>
  </header>
  <center>
<!--     <a href="<?= base_url('index.php/C_dosen/tambah_matkul') ?>" class="btn btn-primary" ><i class="ri-folder-add-line"></i> Tambah Mata Pelajaran</a>&nbsp &nbsp -->
    <?php foreach ($kelas as $kelas ) { ?>    
    <img src="<?= base_url().'uploadfile/'.$kelas['gambar']?>" style="height:100px; width:300px;" ><br>
    <?php } ?>
  </center>
  <br><br>
  <div class="row gy-4">
    <?= $this->session->flashdata('message'); ?> 
   <?php
    foreach($mat as $matkul) {
   ?>
    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
      <a href="<?= base_url('index.php/C_admin/detail_matkul/'.$kelas['id_kelas'].$matkul['id_matkul']) ?>" class="read-more">
      <div class="service-box blue">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $matkul['nama_matkul']?></h3>
        <p><?= $matkul['id_matkul'] ?></p>
      </div>
      </a>
    </div>

  
   <?php } ?>
</div>

</section><!-- End Services Section -->