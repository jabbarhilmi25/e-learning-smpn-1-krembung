
<section id="services" class="services">

<div class="container" data-aos="fade-up">

  <header class="section-header">
  <br><br><br>
  </header>

  <div class="row gy-4">
   <?php
    foreach($tugas as $tugas) {
   ?>

    <div class="col-lg-12 col-md-1" data-aos="fade-up" data-aos-delay="200">
      <div class="service-box purple" style="padding: 20px 10px;">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $tugas['judul_tugas']?></h3>
        <p><?= $tugas['nama_matkul'] ?></p>
      </div>
    </div>
        
   <?= $this->session->flashdata('message'); ?> 
    <div class="card" style="width: 100%;">
  <div class="card-body">
    <h4 class="card-title">Detail Tugas</h4>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">
      <font color="#007aff"><p><i class="ri-calendar-event-fill"></i> Tanggal Dibuat : <?=date("d/m/Y H:i" , strtotime($tugas['tanggal_tugas'])) ?></p></font>
      <font color="red"><p><i class="ri-calendar-check-fill"></i> Deadline : <?=date("d/m/Y H:i" , strtotime($tugas['deadline'])) ?></p></font>
      <br>
      <p><?= nl2br($tugas['deskripsi_tugas']) ?></p><br>
      <?php if($tugas['file_tugas'] != NULL ) { ?>
      <div class="form-control" style=" border-style: solid; border-width: 3px;"><strong><p>Download file dibawah ini !</p></strong>
      <a href="<?php echo base_url('index.php/C_dosen/download/'.$tugas['file_tugas'])?>" class="btn btn-dark btn-sm"><i class="ri-file-download-line"></i>  Download File</a> &nbsp <?= $tugas['file_tugas'] ?>
      <?php } ?>
      </div><br>
        
    </li>

    <?php } ?>
</div>

</div>

<!-- <script>
    document.onkeydown=function(evt){
        var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
        if(keyCode == 13)
        {
            //your function call here
            document.test.submit();
        }
    }
</script>  -->

</section><!-- End Services Section