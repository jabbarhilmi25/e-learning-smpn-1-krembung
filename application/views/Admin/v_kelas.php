<!-- ======= Services Section ======= -->
<section id="services" class="services">

<br><br><br>
<div class="container" data-aos="fade-up">

  <header class="section-header">
    <p>Daftar Kelas</p>
  </header>
   <center>
    <a href="<?= base_url('index.php/C_dosen/tambah_matkul') ?>" class="btn btn-primary" data-toggle="modal" data-target="#modal-edit" ><i class="ri-folder-add-line"></i> Tambah Kelas</a>&nbsp &nbsp
  </center> 
  <br><br>
  <div class="row gy-4">
    <?= $this->session->flashdata('message'); ?> 

    <?php
    //Columns must be a factor of 12 (1,2,3,4,6,12)
    $numOfCols = 4;
    $rowCount = 0;
    $bootstrapColWidth = 12 / $numOfCols;
    ?>
    <div class="row">
    <?php
    foreach ($kelas as $kls){
    ?>  
            <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                <div class="thumbnail">
                    <center>
                      <br><br>
                      <a href="<?= base_url('index.php/C_admin/show_matkul/'.$kls['id_kelas']) ?>">
                      <img src="<?= base_url().'uploadfile/'.$kls['gambar']?>" style="height:100px; width:300px;" >
                      </a>
                      <br><br>
                    </center>
                </div>
            </div>
    <?php
        $rowCount++;
        if($rowCount % $numOfCols == 0) echo '</div><br><div class="row">';
    }
    ?>
    </div>

</div>

</section><!-- End Services Section -->

<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-dark">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Kelas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('index.php/C_dosen/simpan_kelas') ?>" method="post" enctype="multipart/form-data">
      <div class="modal-body">
        <label>Nama Kelas</label>
        <input type="text" class="form-control" name="nama" value="" required oninvalid="this.setCustomValidity('Data Tidak Boleh Kosong')" oninput="this.setCustomValidity('')" ><br>
        <label>Wali Kelas</label>
        <select id="list" name="list" class="form-control" id="exampleFormControlSelect1" >
          <option>Pilih Wali Kelas</option>
          <?php
            $list = $this->M_matkul->get_guru();
            foreach ($list as $list) {?>
            <option value="<?=$list['id_guru'];?>"><?= $list['nama_guru'];?></option>
                
            <?php }
          ?>
                      
        </select><br>

        <label>Gambar</label>
        <input type="file" class="form-control" name="file" value=""><br>
        
      </div>
      <div class="modal-footer">
        <div class="form-group" align="right">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>&nbsp
          <button type="submit" class="btn btn-primary">Simpan </button>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>