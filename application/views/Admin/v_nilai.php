<!-- ======= Services Section ======= -->
<section id="services" class="services">

<div class="container" data-aos="fade-up">

  <header class="section-header">
    
    <p>Daftar Nilai</p>
  </header>
  <br><br>
  <div class="row gy-4">
    <?= $this->session->flashdata('message'); ?> 
   <?php
    foreach($mat as $matkul) {
   ?>
    <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
      <a href="<?= base_url('index.php/C_admin/detail_nilai/'.$matkul['id_matkul']) ?>" class="read-more">
      <div class="service-box red">
        <i class="ri-discuss-line icon"></i>
        <h3><?= $matkul['nama_matkul']?></h3>
        <p><?= $matkul['id_matkul'] ?></p>
      </div>
      </a>
    </div>

  
   <?php } ?>
</div>

</section><!-- End Services Section -->