<?php

class M_akun extends CI_model {

	public function get($id){
        $this->db->where('id_akun', $id); 
        $result = $this->db->get('akun')->row_array(); 
        return $result;
    }

    public function get_akunSiswa(){
        $this->db->SELECT('*');
		$this->db->FROM('murid');
		$query = $this->db->get();
    	return $query->result_array();
    }

    public function get_idSiswa_byId($id){
        $this->db->where('id_murid', $id); 
        $result = $this->db->get('murid')->row_array(); 
        return $result;
     }

    public function get_akunGuru(){
        $this->db->SELECT('*');
		$this->db->FROM('guru');
		$query = $this->db->get();
    	return $query->result_array();
    }

    public function get_idGuru_byId($id){
        $this->db->where('id_guru', $id); 
        $result = $this->db->get('guru')->row_array(); 
        return $result;
    }

    public function get_idAdmin_byId($id){
        $this->db->where('id_admin', $id); 
        $result = $this->db->get('admin')->row_array(); 
        return $result;
    }

  //   public function get_akun(){
  //   	$this->db->SELECT('*');
		// $this->db->FROM('murid');
		// $this->db->where('level !=', '1'); 
		// $this->db->order_by('level','ASC');
		// $query = $this->db->get();
  //   	return $query->result_array();
  //   }

    public function get_by_id($id){
       $this->db->SELECT('*');
        $this->db->FROM('akun');
        $this->db->where('id_akun',$id); 
        $query = $this->db->get();
        return $query->result_array();
    }

    public function edit_guru($data){
        $this->db->where('id_guru', $data['id_guru']);
        $this->db->update('guru' , $data);
    }

    public function edit_murid($data){
        $this->db->where('id_murid', $data['id_murid']);
        $this->db->update('murid' , $data);
    }

    public function tambah($data){
        $this->db->insert('akun' , $data);
    }

}
?>
