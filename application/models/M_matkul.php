<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_matkul extends CI_Model{

    public function get_akun(){
        $this->db->SELECT('*');
        $this->db->FROM('murid');
        $this->db->order_by('id_murid','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_akun_by_id($id){
        $query = $this->db->query("SELECT * FROM murid
            WHERE (id_murid LIKE '%$id%' OR nama_murid LIKE '%$id%')");  
        return $query->result_array();
    }

    public function get_matkul(){
        $this->db->SELECT('*');
		$this->db->FROM('mata_kuliah');
		$this->db->order_by('id_matkul','ASC');
		$query = $this->db->get();
    	return $query->result_array();
    }

    public function get_matkul_byid($id){
        $this->db->SELECT('*');
        $this->db->FROM('mata_kuliah');
        $this->db->order_by('id_matkul','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_kelas(){
        $this->db->SELECT('*');
        $this->db->FROM('kelas');
        $this->db->order_by('nama_kelas','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_kelas_byid($id){
        $this->db->SELECT('*');
        $this->db->FROM('kelas');
        $this->db->where('id_kelas',$id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_guru(){
        $this->db->SELECT('*');
        $this->db->FROM('guru');
        $this->db->order_by('id_guru','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

	public function get_detail($id){
        $this->db->SELECT('*');
		$this->db->FROM('mata_kuliah');
		$this->db->where('id_matkul',$id);
		$query = $this->db->get();
    	return $query->result_array();
    }

	public function get_tugas($id,$kelas){
        $this->db->SELECT('*');
		$this->db->FROM('tugas');
		$this->db->where('id_matkul',$id);
        $this->db->where('id_kelas',$kelas);
		$this->db->order_by('tanggal_tugas','ASC');
		$query = $this->db->get();
    	return $query->result_array();
    }

    public function get_nilai_tugas($id){
        $this->db->SELECT('*');
        $this->db->FROM('tugas');
        $this->db->where('id_matkul',$id);
        $this->db->order_by('tanggal_tugas','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function tambah($data){
        $this->db->insert('mata_kuliah' , $data);
    }

    public function tambah_kelas($data){
        $this->db->insert('kelas' , $data);
    }

    public function edit($data){
        $this->db->where('id_matkul', $data['id_matkul']);
        $this->db->update('mata_kuliah' , $data);
    }

    public function detail_tugas($id){
        $this->db->select('mk.*,
                            tg.*,');
        $this->db->from('tugas tg');
        $this->db->join('mata_kuliah mk', 'mk.id_matkul = tg.id_matkul','left');
        $this->db->where('id_tugas', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function diskusi($id){
        $this->db->select('tg.*,
                            m.*,
                            d.*');
        $this->db->from('diskusi d');
        $this->db->join('tugas tg', 'tg.id_tugas = d.id_tugas','left');
        $this->db->join('murid m', 'm.id_murid= d.id_akun','left');
        $this->db->where('d.id_tugas', $id);
        $this->db->order_by('tgl_diskusi','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function tambah_tugas($data){
        $this->db->insert('tugas' , $data);
    }

    public function tambah_komen($data){
        $this->db->insert('diskusi' , $data);
    }

    public function update_komen($data){
        $this->db->where('id_diskusi', $data['id_diskusi']);
        $this->db->update('diskusi' , $data);
    }

    public function delete_komen($data){
        $this->db->where('id_diskusi', $data['id_diskusi']);
        $this->db->delete('diskusi' , $data);
    }   

    public function edit_tugas($data){
        $this->db->where('id_tugas', $data['id_tugas']);
        $this->db->update('tugas' , $data);
    }

    public function upload_jawaban($data){
        $this->db->insert('jawaban', $data);
    }

    public function jawaban($id){
        $this->db->select('j.*,
                            m.*,t.*');
        $this->db->from('jawaban j');
        $this->db->join('murid m', 'm.id_murid = .j.id_murid','left');
        $this->db->join('tugas t', 't.id_tugas = .j.id_tugas','left');
        $this->db->where('j.id_tugas', $id);
        $this->db->order_by('tanggal_jawaban','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function update_nilai($data){
        $this->db->where('id_jawaban', $data['id_jawaban']);
        $this->db->update('jawaban' , $data);
    }

    public function getjawaban_byakun($tg,$id){
        $this->db->select('j.*,
                            m.*,t.*');
        $this->db->from('jawaban j');
        $this->db->join('murid m', 'm.id_murid = j.id_murid','left');
        $this->db->join('tugas t', 't.id_tugas = j.id_tugas','left');
        $this->db->where('j.id_tugas', $tg);
        $this->db->where('j.id_murid', $id);
        $this->db->order_by('tanggal_jawaban','ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_nilai($id,$akun){
        $query = $this->db->query(" 
            SELECT j.*,m.*,t.*,d.*
            FROM jawaban j
            LEFT JOIN murid m ON m.id_murid = j.id_murid
            LEFT JOIN tugas t ON t.id_matkul = j.id_matkul
            LEFT JOIN diskusi d ON d.id_tugas = j.id_tugas
            WHERE j.id_matkul= '$id' AND j.id_murid= '$akun'
            GROUP BY j.id_tugas

        ");
        return $query->result_array();
    }

    public function get_rata($id,$akun){
        $query = $this->db->query(" 
            SELECT AVG(j.nilai_jawaban) as rata
            FROM jawaban j
            WHERE j.id_matkul = '$id' AND j.id_murid = '$akun'
        ");
        return $query->result_array();
    }

    // SELECT j.*,a.*,t.*,SUM(d.poin_diskusi)
    //         FROM jawaban j
    //         LEFT JOIN akun a ON a.id_akun = j.id_akun
    //         LEFT JOIN tugas t ON t.id_matkul = j.id_matkul
    //         LEFT JOIN diskusi d ON d.id_tugas = j.id_tugas
    //         WHERE j.id_matkul= 'SI001' AND j.id_akun= 'david123'
    //         ORDER BY t.tanggal_tugas ASC
}

?>