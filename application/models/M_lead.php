<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class M_lead extends CI_Model{

// public function lead2(){
//         $this->db->select('count(tg.),
//                             a.*,
//                             d.*');
//         $this->db->from('diskusi d');
//         $this->db->join('tugas tg', 'tg.id_tugas = d.id_tugas','left');
//         $this->db->join('akun a', 'a.id_akun = d.id_akun','left');
//         $this->db->where('d.id_tugas', $id);
//         $this->db->order_by('tgl_diskusi','ASC');
//         $query = $this->db->get();
//         return $query->result_array();
//     }

    public function lead($id){ 
        $query = $this->db->query("
        SELECT m.*,mk.*,
        CASE
        WHEN d.id_matkul = j.id_matkul THEN AVG(d.poin_diskusi)+AVG(j.nilai_jawaban) 
        ELSE AVG(j.nilai_jawaban)
        END AS hasil
        FROM jawaban j
        LEFT JOIN murid m ON j.id_murid=m.id_murid
        LEFT JOIN diskusi d ON d.id_akun=m.id_murid AND d.id_matkul = j.id_matkul
        LEFT JOIN tugas t ON t.id_tugas=j.id_tugas
        LEFT JOIN mata_kuliah mk ON mk.id_matkul=t.id_matkul
        WHERE t.id_matkul = '$id' 
        GROUP BY m.id_murid
        ORDER BY hasil DESC
        ");
        return $query->result_array();
    }

    public function leader($id){

        $query = $this->db->query("
        SELECT d.*, j.*, a.* FROM diskusi d, jawaban j, akun a WHERE d.id_matkul = '$id' AND  d.id_matkul = j.id_matkul GROUP BY a.id_akun
        ");
        return $query->result_array();
    }


    // public function lead2(){
    //     $this->db->select('count(tg.),
    //                         a.*,
    //                         d.*');
    //     $this->db->from('diskusi d');
    //     $this->db->join('tugas tg', 'tg.id_tugas = d.id_tugas','left');
    //     $this->db->join('akun a', 'a.id_akun = d.id_akun','left');
    //     $this->db->where('d.id_tugas', $id);
    //     $this->db->order_by('tgl_diskusi','ASC');
    //     $query = $this->db->get();
    //     return $query->result_array();
    // }

    // SELECT SUM(d.poin_diskusi)+j.nilai_jawaban AS hasil, a.*,m.*
    //     FROM akun a
    //     LEFT JOIN diskusi d ON d.id_akun=a.id_akun
    //     LEFT JOIN jawaban j ON j.id_akun=a.id_akun
    //     LEFT JOIN tugas t ON 
    //     (CASE 
    //     WHEN d.id_matkul = '$id' AND d.id_tugas = t.id_tugas THEN t.id_tugas=j.id_tugas AND t.id_tugas=d.id_tugas
    //     WHEN d.id_matkul = '$id' AND d.id_tugas != t.id_tugas THEN t.id_tugas=j.id_tugas
    //     END
    //     )     
    //     LEFT JOIN mata_kuliah m ON m.id_matkul=t.id_matkul
    //     WHERE a.level = '3'AND t.id_matkul = '$id' 
    //     GROUP BY id_akun
    //     ORDER BY hasil DESC


// SELECT SUM(d.poin_diskusi)+j.nilai_jawaban, a.*,m.nama_matkul
//     FROM akun a
//     LEFT JOIN diskusi d ON d.id_akun=a.id_akun
//     LEFT JOIN jawaban j ON j.id_akun=a.id_akun
//     LEFT JOIN tugas t ON t.id_tugas=j.id_tugas AND d.id_tugas 
//     LEFT JOIN mata_kuliah m ON m.id_matkul=t.id_matkul
//     GROUP BY id_akun

// SELECT SUM(d.poin_diskusi)+SUM(j.nilai_jawaban), a.*
//     FROM akun a
//     LEFT JOIN diskusi d ON d.id_akun=a.id_akun
//     LEFT JOIN jawaban j ON j.id_akun=a.id_akun
//     GROUP BY id_akun

// betul SELECT SUM(d.poin_diskusi)+j.nilai_jawaban, a.id_akun,m.nama_matkul
//     FROM akun a
//     LEFT JOIN diskusi d ON d.id_akun=a.id_akun
//     LEFT JOIN jawaban j ON j.id_akun=a.id_akun
//     LEFT JOIN tugas t ON t.id_tugas=j.id_tugas
//     LEFT JOIN mata_kuliah m ON m.id_matkul=t.id_matkul
//     WHERE a.level = '3'
//     GROUP BY id_akun

// SELECT SUM(d.poin_diskusi+j.nilai_jawaban), a.*
//     FROM akun a
//     LEFT JOIN diskusi d ON d.id_akun=a.id_akun
//     LEFT JOIN jawaban j ON j.id_akun=a.id_akun
//     WHERE d.id_akun='tera123'
//     GROUP BY j.nilai_jawaban

    // SET @a1 = (SELECT SUM(d.poin_diskusi)
    //                 FROM diskusi d
    //                 GROUP BY d.id_akun);

// SET @a2 = (SELECT SUM(j.nilai_jawaban)
//                 FROM jawaban j
//                 GROUP BY j.id_akun);
          
// SET @hasil = @a1 + @a2;

// SELECT @hasil

// SELECT SUM(d.poin_diskusi)+j.nilai_jawaban AS hasil, a.*,m.*
//         FROM akun a
//         LEFT JOIN diskusi d ON d.id_akun=a.id_akun
//         LEFT JOIN jawaban j ON j.id_akun=a.id_akun
//         LEFT JOIN tugas t ON 
//         (CASE 
//         WHEN d.id_tugas = t.id_tugas THEN t.id_tugas=j.id_tugas AND t.id_tugas=d.id_tugas
//         WHEN d.id_tugas != t.id_tugas THEN t.id_tugas=j.id_tugas
//         END
//         )     
//         LEFT JOIN mata_kuliah m ON m.id_matkul=t.id_matkul
//         WHERE a.level = '3'AND t.id_matkul = 'AK001' 
//         GROUP BY id_akun
//         ORDER BY hasil DESC

// SELECT SUM(d.poin_diskusi)+j.nilai_jawaban AS hasil, a.*,m.*
//     FROM akun a
//     LEFT JOIN diskusi d ON d.id_akun=a.id_akun
//     LEFT JOIN jawaban j ON j.id_akun=a.id_akun
//     LEFT JOIN tugas t ON 
//     (CASE 
//     WHEN d.id_matkul = 'AK001' AND d.id_tugas = t.id_tugas THEN t.id_tugas=j.id_tugas AND t.id_tugas=d.id_tugas
//     WHEN d.id_matkul != 'AK001' AND d.id_tugas != t.id_tugas THEN t.id_tugas=j.id_tugas
//     END
//     )     
//     LEFT JOIN mata_kuliah m ON m.id_matkul=t.id_matkul
//     WHERE a.level = '3'AND t.id_matkul = 'AK001' 
//     GROUP BY id_akun
//     ORDER BY hasil DESC

    // SELECT AVG(d.poin_diskusi)+AVG(j.nilai_jawaban) AS hasil, a.*,m.*,AVG(d.poin_diskusi),AVG(j.nilai_jawaban)
    //     FROM jawaban j
    //     LEFT JOIN akun a ON j.id_akun=a.id_akun
    //     LEFT JOIN diskusi d ON d.id_akun=a.id_akun AND d.id_matkul = j.id_matkul
    //     LEFT JOIN tugas t ON t.id_tugas=j.id_tugas
    //     LEFT JOIN mata_kuliah m ON m.id_matkul=t.id_matkul
    //     WHERE a.level = '3'AND t.id_matkul = 'AK001' 
    //     GROUP BY id_akun
    //     ORDER BY hasil DESC

 //    SELECT a.*,m.*,
 // CASE
 //    WHEN d.id_matkul = j.id_matkul THEN AVG(d.poin_diskusi)+AVG(j.nilai_jawaban) 
 //    ELSE AVG(j.nilai_jawaban)
 // END AS hasil
 //        FROM jawaban j
 //        LEFT JOIN akun a ON j.id_akun=a.id_akun
 //        LEFT JOIN diskusi d ON d.id_akun=a.id_akun AND d.id_matkul = j.id_matkul
 //        LEFT JOIN tugas t ON t.id_tugas=j.id_tugas
 //        LEFT JOIN mata_kuliah m ON m.id_matkul=t.id_matkul
 //        WHERE a.level = '3'AND t.id_matkul = 'SI002' 
 //        GROUP BY id_akun
 //        ORDER BY hasil DESC
}
?>