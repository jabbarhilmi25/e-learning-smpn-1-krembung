-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Jun 2021 pada 03.33
-- Versi server: 10.4.18-MariaDB
-- Versi PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearning1`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` varchar(10) NOT NULL,
  `nama_admin` varchar(20) DEFAULT NULL,
  `password_admin` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `password_admin`) VALUES
('001', 'Sudirman', 'dc5c7986daef50c1e02ab09b442ee34f');

-- --------------------------------------------------------

--
-- Struktur dari tabel `diskusi`
--

CREATE TABLE `diskusi` (
  `id_diskusi` varchar(10) NOT NULL,
  `tgl_diskusi` datetime NOT NULL,
  `isi_diskusi` varchar(500) NOT NULL,
  `poin_diskusi` int(2) DEFAULT NULL,
  `id_akun` varchar(20) NOT NULL,
  `id_tugas` varchar(10) NOT NULL,
  `id_matkul` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `diskusi`
--

INSERT INTO `diskusi` (`id_diskusi`, `tgl_diskusi`, `isi_diskusi`, `poin_diskusi`, `id_akun`, `id_tugas`, `id_matkul`) VALUES
('DIS001', '2021-04-09 09:45:44', 'Setelah menikah, setiap istri tentara wajib mengikuti peraturan yang telah dibakukan dalam lingkungan masing-masing matra tempat suami mereka terdaftar dan menjadi anggota dalam persatuan istri tentara (Persit) di matra tersebut.', -5, '001', 'TGS001', 'SI001'),
('DIS002', '2021-04-09 17:52:16', 'Listen ad-free with YouTube Premium Song Sesuatu Di Jogja Artist Adhitia Sofyan Licensed to YouTube by TuneCore (on behalf of Quiet Down Records); TuneCore Publishing, BMI - Broadcast Music Inc., and 2 Music Rights Societies', 20, '001', 'TGS001', 'SI001'),
('DIS003', '2021-04-22 14:46:18', '#animenia​#onepiece​\r\nSpoiler One Piece chapter 1011 versi detaillll\r\n\r\nMr.K baca One Piece disini :\r\nVIZ SHOUNEN JUMP(U.S) https://www.viz.com/apps ', 5, '001', 'TGS002', 'AK001'),
('DIS004', '2021-04-22 14:46:40', '#animenia​#onepiece​\r\nSpoiler One Piece chapter 1011 versi detaillll\r\n\r\nMr.K baca One Piece disini :\r\nVIZ SHOUNEN JUMP(U.S) https://www.viz.com/apps ', 5, '002', 'TGS002', 'AK001'),
('DIS005', '2021-04-22 14:46:57', '#animenia​#onepiece​\r\nSpoiler One Piece chapter 1011 versi detaillll\r\n\r\nMr.K baca One Piece disini :\r\nVIZ SHOUNEN JUMP(U.S) https://www.viz.com/apps ', 10, '002', 'TGS002', 'AK001'),
('DIS006', '2021-04-23 13:08:18', 'hgadagfae', 10, '003', 'TGS004', 'SI002'),
('DIS007', '2021-06-02 18:07:16', 'test\r\n', 5, '001', 'TGS001', '');

--
-- Trigger `diskusi`
--
DELIMITER $$
CREATE TRIGGER `diskusiauto` BEFORE INSERT ON `diskusi` FOR EACH ROW BEGIN
DECLARE nr integer DEFAULT 0;


SELECT COUNT(*) INTO @id FROM diskusi;

set new.id_diskusi =concat("DIS", LPAD(@id+1,3,'0'));


END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id_guru` varchar(10) NOT NULL,
  `nama_guru` varchar(20) DEFAULT NULL,
  `password_guru` varchar(60) DEFAULT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id_guru`, `nama_guru`, `password_guru`, `level`) VALUES
('gr001', 'oemar bakri', '827ccb0eea8a706c4c34a16891f84e7b', 2),
('gr002', 'soeharto', '25d55ad283aa400af464c76d713c07ad', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jawaban`
--

CREATE TABLE `jawaban` (
  `id_jawaban` varchar(10) NOT NULL,
  `id_tugas` varchar(10) NOT NULL,
  `id_matkul` varchar(10) NOT NULL,
  `id_murid` varchar(15) NOT NULL,
  `tanggal_jawaban` datetime NOT NULL,
  `file_jawaban` varchar(100) DEFAULT NULL,
  `nilai_jawaban` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jawaban`
--

INSERT INTO `jawaban` (`id_jawaban`, `id_tugas`, `id_matkul`, `id_murid`, `tanggal_jawaban`, `file_jawaban`, `nilai_jawaban`) VALUES
('JWB001', 'TGS001', 'MK001', '001', '2021-06-02 18:06:44', 'soal_latihan.docx', 90),
('JWB002', 'TGS002', 'MK001', '001', '2021-06-04 09:42:22', 'alur.docx', 71);

--
-- Trigger `jawaban`
--
DELIMITER $$
CREATE TRIGGER `idautojawaban` BEFORE INSERT ON `jawaban` FOR EACH ROW BEGIN
DECLARE nr integer DEFAULT 0;


SELECT COUNT(*) INTO @id FROM jawaban;

set new.id_jawaban =concat("JWB", LPAD(@id+1,3,'0'));


END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` varchar(10) NOT NULL,
  `id_guru` varchar(10) DEFAULT NULL,
  `nama_kelas` varchar(10) DEFAULT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `id_guru`, `nama_kelas`, `gambar`) VALUES
('kl001', 'gr001', 'Kelas 7A', 'kelas5.png'),
('kl002', 'gr002', 'Kelas 7B', 'kelas6.png'),
('kl003', 'gr001', 'Kelas 8A', 'kelas1.png'),
('kl004', 'gr002', 'Kelas 8B', 'kelas2.png'),
('kl005', 'gr001', 'Kelas 9A', 'kelas3.png'),
('kl006', 'gr001', 'Kelas 9B', 'kelas4.png'),
('kl007', 'gr002', 'Kelas 7C', 'kelas7.png'),
('kl008', 'gr001', 'Kelas 8C', '49103579204-meja-warnet.jpg');

--
-- Trigger `kelas`
--
DELIMITER $$
CREATE TRIGGER `idautokelas` BEFORE INSERT ON `kelas` FOR EACH ROW BEGIN
DECLARE nr integer DEFAULT 0;


SELECT COUNT(*) INTO @id FROM kelas;

set new.id_kelas =concat("kl", LPAD(@id+1,3,'0'));


END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `mata_kuliah`
--

CREATE TABLE `mata_kuliah` (
  `id_matkul` varchar(20) NOT NULL,
  `nama_matkul` varchar(20) NOT NULL,
  `jam_belajar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mata_kuliah`
--

INSERT INTO `mata_kuliah` (`id_matkul`, `nama_matkul`, `jam_belajar`) VALUES
('MK001', 'Bahasa Indonesia', 4),
('MK002', 'Matematika', 6);

--
-- Trigger `mata_kuliah`
--
DELIMITER $$
CREATE TRIGGER `idmkauto` BEFORE INSERT ON `mata_kuliah` FOR EACH ROW BEGIN
DECLARE nr integer DEFAULT 0;


SELECT COUNT(*) INTO @id FROM mata_kuliah;

set new.id_matkul =concat("MK", LPAD(@id+1,3,'0'));


END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `murid`
--

CREATE TABLE `murid` (
  `id_murid` varchar(10) NOT NULL,
  `id_kelas` varchar(10) DEFAULT NULL,
  `nama_murid` varchar(20) DEFAULT NULL,
  `password_murid` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `murid`
--

INSERT INTO `murid` (`id_murid`, `id_kelas`, `nama_murid`, `password_murid`) VALUES
('001', 'kl001', 'habibie', '25f9e794323b453885f5181f1b624d0b');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tugas`
--

CREATE TABLE `tugas` (
  `id_tugas` varchar(10) NOT NULL,
  `judul_tugas` varchar(50) NOT NULL,
  `id_matkul` varchar(15) NOT NULL,
  `id_kelas` varchar(10) NOT NULL,
  `tanggal_tugas` datetime NOT NULL,
  `deadline` datetime NOT NULL,
  `file_tugas` varchar(100) DEFAULT NULL,
  `deskripsi_tugas` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tugas`
--

INSERT INTO `tugas` (`id_tugas`, `judul_tugas`, `id_matkul`, `id_kelas`, `tanggal_tugas`, `deadline`, `file_tugas`, `deskripsi_tugas`) VALUES
('TGS001', 'Pemodelan Teks Laporan Hasil Observasi', 'MK001', 'kl001', '2021-05-31 12:31:28', '0000-00-00 00:00:00', NULL, 'Pada Kegiatan 1 ini kamu diajak mengenali dan memahami teks laporan hasil observasi. Teks yang digunakan untuk belajar berjudul “Cinta Lingkungan”. Pada kegiatan ini kamu harus mengerjakan beberapa tugas. Untuk membangun konteks dan pemahaman kamu tentang lingkungan hidup, jawablah beberapa pertanyaan berikut!\r\n1) Ceritakanlah keadaan lingkungan yang ada di sekitar rumahmu!\r\n2) Bagaimanakah menurutmu alam Indonesia yang kita cintai?\r\n3) Kekayaan apakah yang ada di dalam bumi Indonesia?\r\n4) kekay'),
('TGS002', 'Buatlah Teks anekdot', 'MK001', 'kl001', '2021-06-02 15:42:20', '2021-06-16 00:00:00', 'EKSTERNALITAS_PABRIK_POTONG_AYAM_CV_REV.docx', 'Buatlah teks anekdot berdasarkan file ini');

--
-- Trigger `tugas`
--
DELIMITER $$
CREATE TRIGGER `idauto` BEFORE INSERT ON `tugas` FOR EACH ROW BEGIN
DECLARE nr integer DEFAULT 0;


SELECT COUNT(*) INTO @id FROM tugas;

set new.id_tugas =concat("TGS", LPAD(@id+1,3,'0'));


END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `diskusi`
--
ALTER TABLE `diskusi`
  ADD PRIMARY KEY (`id_diskusi`);

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indeks untuk tabel `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indeks untuk tabel `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`),
  ADD KEY `id_guru` (`id_guru`);

--
-- Indeks untuk tabel `mata_kuliah`
--
ALTER TABLE `mata_kuliah`
  ADD PRIMARY KEY (`id_matkul`);

--
-- Indeks untuk tabel `murid`
--
ALTER TABLE `murid`
  ADD PRIMARY KEY (`id_murid`),
  ADD KEY `id_kelas` (`id_kelas`);

--
-- Indeks untuk tabel `tugas`
--
ALTER TABLE `tugas`
  ADD PRIMARY KEY (`id_tugas`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
