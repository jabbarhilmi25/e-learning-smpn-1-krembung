$(document).ready(function () {
	get_diskusi();

	$("#form_1").submit(function (e) {
		e.preventDefault();

		$.ajax({
			url: base_url + "C_dosen/simpan_komen/",
			type: "post",
			dataType: "json",
			data: $(this).serialize(),
			success: function (data) {
				if (data.status) {
					$("#komen").val("");
				}
			},
		});
	});

	$(document).on("click", ".edit", function () {
		var id = $(this).data("id");

		$.ajax({
			url: base_url + "C_dosen/data_update",
			type: "post",
			dataType: "json",
			data: { id: id },
			success: function (data) {
				$("#target_form_update").html(data);
			},
		});
	});

	$("#form-update").submit(function (e) {
		e.preventDefault();

		$.ajax({
			url: base_url + "C_dosen/edit_komen",
			type: "post",
			dataType: "json",
			data: $(this).serialize(),
			success: function (data) {
				if (data["status"]) {
					// $("#modal-edit").modal("hide");
					// $("#modal-edit").hide();
					// $(".modal-backdrop").hide();
					toastr.success(data.pesan);
					setTimeout(function () {
						location.reload();
					}, 1000);
				}
			},
		});
	});

	$(document).on("click", ".hapus", function () {
		var id = $(this).data("id");
		$("#id_hapus").val(id);
	});

	$("#form-delete").submit(function (e) {
		e.preventDefault();

		$.ajax({
			url: base_url + "C_dosen/hapus_komen",
			type: "post",
			dataType: "json",
			data: $(this).serialize(),
			success: function (data) {
				if (data.status) {
					// $("#modal-hapus").hide();
					// $(".modal-backdrop").hide();
					toastr.success(data.pesan);
					setTimeout(function () {
						location.reload();
					}, 1000);
				}
			},
		});
	});

	$(document).on("click", ".point10", function (e) {
		e.preventDefault();
		var id = $(this).data("id");

		$.ajax({
			url: base_url + "C_dosen/poin_komen",
			type: "post",
			dataType: "json",
			data: { id: id, poin: 10 },
			success: function (data) {
				if (data.status) {
					toastr.success(data.pesan);
				} else {
					toastr.error(data.pesan);
				}
			},
		});
	});

	$(document).on("click", ".point20", function (e) {
		e.preventDefault();
		var id = $(this).data("id");
		$.ajax({
			url: base_url + "C_dosen/poin_komen",
			type: "post",
			dataType: "json",
			data: { id: id, poin: 20 },
			success: function (data) {
				if (data.status) {
					toastr.success(data.pesan);
				} else {
					toastr.error(data.pesan);
				}
			},
		});
	});

	$(document).on("click", ".report", function (e) {
		e.preventDefault();
		var id = $(this).data("id");
		$.ajax({
			url: base_url + "C_dosen/poin_komen",
			type: "post",
			dataType: "json",
			data: { id: id, poin: -5 },
			success: function (data) {
				if (data.status) {
					toastr.success(data.pesan);
				} else {
					toastr.error(data.pesan);
				}
			},
		});
	});

	function get_diskusi() {
		var id = $("#id").val();
		setInterval(function () {
			$.ajax({
				url: base_url + "C_dosen/get_diskusi",
				type: "post",
				dataType: "json",
				data: { id: id },
				success: function (data) {
					$("#isi_diskusi").html(data);
				},
			});
		}, 2000);
	}
});
