$(document).ready(function () {
	get_diskusi();

	$("#form_1").submit(function (e) {
		e.preventDefault();

		$.ajax({
			url: base_url + "C_mhs/simpan_komen/",
			type: "post",
			dataType: "json",
			data: $(this).serialize(),
			success: function (data) {
				if (data.status) {
					$("#komen").val("");
				}
			},
		});
	});

	$(document).on("click", ".edit", function () {
		var id = $(this).data("id");

		$.ajax({
			url: base_url + "C_mhs/data_update",
			type: "post",
			dataType: "json",
			data: { id: id },
			success: function (data) {
				$("#target_form_update").html(data);
			},
		});
	});

	$("#form-update").submit(function (e) {
		e.preventDefault();

		$.ajax({
			url: base_url + "C_mhs/edit_komen",
			type: "post",
			dataType: "json",
			data: $(this).serialize(),
			success: function (data) {
				if (data["status"]) {
					// $("#modal-edit").modal("hide");
					// $("#modal-edit").hide();
					// $(".modal-backdrop").hide();
					toastr.success(data.pesan);
					setTimeout(function () {
						location.reload();
					}, 1000);
				}
			},
		});
	});

	$(document).on("click", ".hapus", function () {
		var id = $(this).data("id");
		$("#id_hapus").val(id);
	});

	$("#form-delete").submit(function (e) {
		e.preventDefault();

		$.ajax({
			url: base_url + "C_mhs/hapus_komen",
			type: "post",
			dataType: "json",
			data: $(this).serialize(),
			success: function (data) {
				if (data.status) {
					// $("#modal-hapus").hide();
					// $(".modal-backdrop").hide();
					toastr.success(data.pesan);
					setTimeout(function () {
						location.reload();
					}, 1000);
				}
			},
		});
	});

	function get_diskusi() {
		var id = $("#id").val();
		setInterval(function () {
			$.ajax({
				url: base_url + "C_mhs/get_diskusi",
				type: "post",
				dataType: "json",
				data: { id: id },
				success: function (data) {
					$("#isi_diskusi").html(data);
				},
			});
		}, 2000);
	}
});
